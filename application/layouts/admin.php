<?php
	$ol_user = $this->userdata;
?>
<html>
	<head>
		<base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | Cabaran Inovasi Inklusif</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!--    <link rel="shortcut icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">-->
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" media="print" href="<?= ASSETS_CSS ?>media_print.css">
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>bootstrapValidator.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>font-awesome.min.css" />
        <link href="<?= ASSETS_CSS ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>site.css" rel="stylesheet" type="text/css" />
        {{styles}}
	</head>
	<body class="container-fluid">
        <div class="header foot-menu margin-btm-20">
                <nav class="menu navbar">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar text-white"></span>
                            <span class="icon-bar text-white"></span>
                            <span class="icon-bar text-white"></span>
                        </button>
                        <a class="navbar-brand" href="<?= base_url() ?>">
                            <img alt="Cabaran Inovasi Inklusif" src="<?= base_url().ASSETS_IMG.'logo-cabaran-inklusif.png' ?>" height="100%">
                            <img alt="Inovasi Inklusif" src="<?= base_url().ASSETS_IMG.'logo-inovasi-inklusif.png' ?>" height="62%">
                        </a>
                    </div>
		  <?php if($ol_user){ ?>  
                    <div id="navbar-header" class="collapse navbar-collapse">
                        <?php if($this->menu != "site" && $this->menu != "account"){ ?>
                            <ul class="nav navbar-nav">
                                <li class="<?= $this->menu == "application" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'applications' ?>">Penyertaan</a></li>
                                <?php if($ol_user['role_id'] != USER_ROLE_DISTRICT_OFFICER){ ?>
                                    <li class="<?= $this->menu == "user" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'users' ?>">Pengguna</a></li>
                                    <li class="<?= $this->menu == "innovator" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'innovators' ?>">Inovator</a></li>
                                    <li class="<?= $this->menu == "article" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'articles' ?>">Artikel</a></li>
                                    <li class="<?= $this->menu == "evaluator_group" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'evaluator_groups' ?>">Kumpulan Penilai</a></li>
                                    <li class="<?= $this->menu == "evaluators" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'evaluators' ?>">Penilai</a></li>
                                    <li class="<?= $this->menu == "evaluation_form" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'evaluation_form' ?>">Borang Penilai</a></li>
                                    <li class="dropdown <?= $this->menu == "report" ? "active" : "" ?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?= base_url().PATH_TO_ADMIN.'reports/submission_number_by_date' ?>">Penyertaan Mengikut Tarikh</a></li>
                                            <li><a href="<?= base_url().PATH_TO_ADMIN.'reports/submission_number_by_category' ?>">Penyertaan Mengikut Kategori</a></li>
                                            <li><a href="<?= base_url().PATH_TO_ADMIN.'reports/submission_number_by_zone' ?>">Penyertaan Mengikut Zon</a></li>
                                            <li><a href="<?= base_url().PATH_TO_ADMIN.'reports/submission_by_district' ?>">Penyertaan Berdasarkan Daerah</a></li>
                                            <li><a href="<?= base_url().PATH_TO_ADMIN.'locators/innovator' ?>">Inovator Berdasarkan Taburan</a></li>
                                            <li><a href="<?= base_url().PATH_TO_ADMIN.'locators/application' ?>">Penyertaan Berdasarkan Taburan</a></li>
                                            <li><a href="<?= base_url().PATH_TO_ADMIN.'reports/export_complete_application_data' ?>">Maklumat Penuh Penyertaan</a></li>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <li class="<?= $this->menu == "profile" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'profile' ?>">Kemaskini Profil</a></li>
                            </ul>
                        
                            <ul class="nav navbar-nav navbar-right logout-btn">
                                <li>
                                    <a href="<?= base_url().PATH_TO_ADMIN.'accounts/logout' ?>"><span class="fa fa-sign-out"></span>Log Keluar</a>
                                </li>
                            </ul> 
                        <?php } ?>
                    </div>
            <?php } ?>
            </nav>
        </div>
		<div class="col-xs-12 col-sm-12 col-md-12 content">
	        {{content}}
	    </div>
        
        <div class="col-md-12 col-xs-12 col-sm-12 footer-default text-center">
            <?php $this->load->view('partial/footer') ?>
        </div>
        
	    <script type="text/javascript">
	        var baseUrl = "<?=base_url()?>";
            var adminUrl = "<?=base_url().PATH_TO_ADMIN?>";
	    </script>
	     <!-- jQuery -->
	    <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
	    <!-- Bootstrap -->
	    <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
	    <!-- DATA TABES SCRIPT -->
	    <script src="<?= ASSETS_JS ?>plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>bootbox.min.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>moment.min.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>date.format.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>common.js" type="text/javascript"></script>
	    {{scripts}} 
	</body>
</html>