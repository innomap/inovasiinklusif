<?php
	$ol_user = $this->user_session->get_user();
?>
<html>
	<head>
		<base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | Cabaran Inovasi Inklusif</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!--    <link rel="shortcut icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">-->
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>bootstrapValidator.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>font-awesome.min.css" />
        <link href="<?= ASSETS_CSS ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>site.css" rel="stylesheet" type="text/css" />
        {{styles}}
	</head>
	<body class="container-fluid">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.7";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding footer">
            <div class="col-xs-12 col-sm-12 col-md-12 foot-menu">
                <nav class="navbar navbar-static-top">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-footer" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand <?= (!$ol_user ? 'padding-left-100' : '') ?>" href="<?= base_url() ?>">
                            <img alt="Cabaran Inovasi Inklusif" src="<?= base_url().ASSETS_IMG.'logo-cabaran-inklusif.png' ?>" height="100%">
                            <img alt="Inovasi Inklusif" src="<?= base_url().ASSETS_IMG.'logo-inovasi-inklusif.png' ?>" height="62%">
                        </a>
                    </div>
                    <div id="navbar-footer" class="collapse navbar-collapse">
                        <?php if(!$ol_user){ ?>
                            <ul class="nav navbar-nav">
                                <?php foreach ($this->header_menu as $key => $value) { ?>
                                    <li class="<?= $this->uri->segment(3) == $value['url'] ? 'active' : '' ?>"><a href="<?= base_url().'site/article/'.$value['url'] ?>"><?= $value['menu'] ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php }else{ ?>
                            <?php if($this->menu != "site" && $this->menu != "account"){ ?>
                                <ul class="nav navbar-nav">
                                    <li class="<?= $this->menu == "application" ? "active" : "" ?>"><a href="<?= base_url().'applications' ?>">Penyertaan</a></li>
                                    <li class="<?= $this->menu == "profile" ? "active" : "" ?>"><a href="<?= base_url().'profile' ?>">Kemaskini Profil</a></li>
                                </ul>
                            
                                <ul class="nav navbar-nav navbar-right logout-btn">
                                    <li>
                                        <a href="<?= base_url().'accounts/logout' ?>"><span class="fa fa-sign-out"></span>Log Keluar</a>
                                    </li>
                                </ul> 
                            <?php } ?>
                        <?php } ?>
                    </div>
                </nav>
            </div>
        </div>

		<div class="col-xs-12 col-sm-12 col-md-12 content">
	        {{content}}
	    </div>
        
        <div class="col-md-12 col-sm-12 col-xs-12 footer-default text-center">
            <?php $this->load->view('partial/footer') ?>
        </div>
        
	     <!-- jQuery -->
	    <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
	    <!-- Bootstrap -->
	    <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>bootstrap-filestyle.min.js" type="text/javascript"></script>
	    <!-- DATA TABES SCRIPT -->
	    <script src="<?= ASSETS_JS ?>plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	     <script src="<?= ASSETS_JS ?>bootbox.min.js" type="text/javascript"></script>
         <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBttX1owNNnj65dVwAcp4ICm8d5zCs4vIU"></script>
         <script src="<?= ASSETS_JS ?>common.js" type="text/javascript"></script>
         <script type="text/javascript">
            var baseUrl = "<?=base_url()?>";
        </script>
	    {{scripts}}
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-84640021-1', 'auto');
          ga('send', 'pageview');

        </script>
	</body>
</html>