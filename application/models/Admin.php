<?php

require_once(APPPATH . 'models/General_model.php');
class Admin extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "admin";
		$this->primary_field = "user_id";
	}

	function get_one_join($user_id){
		$this->db->select("admin.*, user.username");
		$this->db->from("admin");
		$this->db->join('user','user.id = admin.user_id');
		$this->db->where('admin.user_id', $user_id);
		$q = $this->db->get();

		return $q->row_array();
	}
}

?>