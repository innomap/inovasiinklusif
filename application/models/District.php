<?php

require_once(APPPATH . 'models/General_model.php');
class District extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "district";
		$this->primary_field = "id";
	}
}

?>