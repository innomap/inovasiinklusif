<?php

require_once(APPPATH . 'models/General_model.php');
class Evaluation extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "evaluation";
		$this->primary_field = "id";
	}

	function get_average_score($where = NULL){
		$arg = "SELECT ROUND(AVG(evaluation.total),1) as score FROM `evaluation` 
				WHERE evaluation.total != 0
					".
					($where != NULL ? " AND {$where}" : "")
					." GROUP BY evaluation.application_id";

		$query = $this->db->query($arg);
            
        return $query;
	}
}

?>