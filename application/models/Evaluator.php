<?php

require_once(APPPATH . 'models/General_model.php');
class Evaluator extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "evaluator";
		$this->primary_field = "user_id";
	}
}

?>