<?php

require_once(APPPATH . 'models/General_model.php');
class Evaluation_detail extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "evaluation_detail";
		$this->primary_field = "id";
	}

	function delete_by_evaluation($evaluation_id){
		$this->db->where("evaluation_id", $evaluation_id);
		return $this->db->delete($this->table_name);
	}
}

?>