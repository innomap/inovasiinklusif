<?php

require_once(APPPATH . 'models/General_model.php');
class User extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "user";
		$this->primary_field = "id";
	}

	function is_username_exist($username, $id = 0){
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where('username',$username);
		if($id != 0){
			$this->db->where('id != ',$id);
		}
		$user = $this->db->get()->row_array();

		if($user != NULL){
			return true;
		}else{
			return false;
		}
	}

	function insert_user($data){
		$data["password"] = $this->get_hash($data["username"], $data["password"]);
		return $this->insert($data);
	}

	function update_user($id, $data){
		if(isset($data['password'])){
			$data["password"] = $this->get_hash($data["username"], $data["password"]);	
		}
		
		return $this->update($id, $data);
	}

	function get_hash($username, $password) {
		return md5($username.':'.$password);
	}

	function update_password($id, $data){
		$data["password"] = $this->get_hash($data["username"], $data["password"]);
		return $this->update($id, $data);
	}

	function get_number($where = NULL){
		$this->db->select("*");
        $this->db->from($this->table_name);
        if($where != NULL){
        	$this->db->where($where);
        }
        $q = $this->db->get();

        return $q->num_rows();
	}

	function get_admin($where = NULL){
		$this->db->select('user.*, admin.*, district.name as district_name');
		$this->db->from('user');
		$this->db->join('admin','user.id = admin.user_id');
		$this->db->join('district', 'admin.district_id = district.id');
		$this->db->where('user.role_id != ', USER_ROLE_INNOVATOR);
		if($where != NULL){
			$this->db->where($where);
		}
		$q = $this->db->get();

		return $q;
	}
}

?>