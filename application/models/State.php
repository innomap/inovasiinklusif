<?php

require_once(APPPATH . 'models/General_model.php');
class State extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "state";
		$this->primary_field = "id";
	}
}

?>