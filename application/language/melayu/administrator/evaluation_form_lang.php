<?php 

	$lang['action'] = "Tindakan";
	$lang['edit'] = "Ubah";
	$lang['delete'] = "Padam";
	$lang['save'] = "Simpan";
	$lang['cancel'] = "Batal";
	$lang['delete_evaluation_focus'] = "Padam Fokus Penilaian";
	$lang['delete_confirm_message'] = "Anda pasti untuk memadam ";
	$lang['new_evaluation_focus'] = "Tambah Fokus Penilaian";
	$lang['percentage'] = "Peratusan";
	$lang['evaluation_focus'] = "Fokus Penilaian";
	$lang['criteria'] = "Kriteria";
	$lang['question'] = "Pertanyaan";
	$lang['max_point'] = "Markah Maksimum";
	$lang['name'] = "Nama";
	$lang['new_evaluation_form'] = "Tambah Borang Penilaian";
	$lang['this_data'] = "data ini";
	$lang['evaluation_form'] = "Borang Penilaian";
	$lang['guidelines'] = "Panduan";
?>