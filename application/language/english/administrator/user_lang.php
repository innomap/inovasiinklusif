<?php
	
	$lang['delete'] = 'Delete';
	$lang['delete_user'] = 'Delete User';
	$lang['cancel'] = 'Cancel';
	$lang['delete_confirm_message'] = 'Are you sure want to delete ';
	$lang['new_user'] = 'New User';
	$lang['username'] = 'Username';
	$lang['name'] = 'Name';
	$lang['role'] = 'Role';
	$lang['action'] = 'Action';
	$lang['edit'] = 'Edit';
	$lang['delete'] = 'Delete';
	$lang['district'] = 'District';
	$lang['form_user'] = 'Form User';
	$lang['password'] = 'Password';
	$lang['retype_password'] = 'Retype Password';
	$lang['ic_number'] = 'IC Number';
	$lang['gender'] = 'Gender';
	$lang['position'] = 'Position';
	$lang['department'] = 'Department';
	$lang['address'] = 'Address';
	$lang['postcode'] = 'Postcode';
	$lang['select_district'] = 'Select District';
	$lang['telp_no'] = 'Telp No';
	$lang['telp_home_no'] = 'Telp No (Home)';
	$lang['save'] = 'Save';
	$lang['change_password'] = 'Change Password';
	$lang['state'] = 'State';
    $lang['add_user'] = 'Add User';
    $lang['edit_user'] = 'Edit User';
    $lang['email'] = 'Email';
    $lang['zone'] = 'Zone';
	$lang['select_zone'] = 'Select Zone';
	$lang['select_state'] = 'Select State';
?>