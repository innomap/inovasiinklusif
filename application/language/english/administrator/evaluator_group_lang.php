<?php
	
	$lang['delete'] = 'Delete';
	$lang['delete_article'] = 'Delete Article';
	$lang['cancel'] = 'Cancel';
	$lang['delete_confirm_message'] = 'Are you sure want to delete ';
	$lang['new_article'] = 'New Article';
	$lang['name'] = 'Name';
	$lang['description'] = 'Description';
	$lang['action'] = 'Action';
	$lang['edit'] = 'Edit';
	$lang['delete'] = 'Delete';
	$lang['form_group'] = 'Form Group';
	$lang['save'] = 'Save';
	$lang['content'] = 'Content';
?>