<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Profile extends Common {

    function __construct() {
        parent::__construct();

        $this->load->model('admin');
        $this->load->model('user');
        $this->load->model('district');
        $this->load->model('zone');
        $this->load->model('state');

        $this->title = "Edit Profile";
        $this->menu = "profile";

        $this->lang->load(PATH_TO_ADMIN.'user',$this->language);

        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
        $this->scripts[] = 'administrator/profile';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
        $data['alert'] = $this->session->flashdata('alert');
        $data['user'] = $this->user->get_admin("user_id = ".$this->userdata['id'])->row_array();
        if($data['user']){
            if($data['user']['ic_number'] != ""){
                $split_ic_num = explode("-", $data['user']['ic_number']);
                $data['user']['ic_num_1'] = $split_ic_num[0];
                $data['user']['ic_num_2'] = $split_ic_num[1];
                $data['user']['ic_num_3'] = $split_ic_num[2];
            }else{
                $data['user']['ic_num_1'] = "";
                $data['user']['ic_num_2'] = "";
                $data['user']['ic_num_3'] = "";
            }
            $district = $this->district->find_one("id = ".$data['user']['district_id']);
            $data['user']['state'] = $this->state->find_one("id = ".$district['state_id']);
        }
        $data['role'] = unserialize(USER_ROLE_ADMIN);
        $data['gender'] = unserialize(GENDER);
        $data['districts'] = $this->district->find_all();
        $data['zone'] = $this->zone->find_all();
        $data['states'] = $this->state->find_all();
        $this->load->view(PATH_TO_ADMIN.'profile/form',$data);
    }

    function save(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
                     
        if($postdata['password']){
            $data["password"] = $postdata['password'];
            $tmp_admin = $this->user->find_by_id($this->userdata['id']);
            $data["username"] = $tmp_admin["username"];
            
            $this->user->update_user($postdata['id'], $data);
        }
    
        if($postdata['id'] > 0){
            $admin = array( "name" => $postdata['name'],
                            "ic_number" => $postdata['ic_num_1']."-".$postdata['ic_num_2']."-".$postdata['ic_num_3'],
                            "gender" => $postdata['gender'],
                            "position" => $postdata['position'],
                            "department" => $postdata['department'],
                            "address" => $postdata['address'],
                            "postcode" => $postdata['postcode'],
                            "telp_home_no" => $postdata['telp_home_no'],
                            "telp_no" => $postdata['telp_no'],
                            );

            if($postdata['id'] > 0){
                if($this->admin->update($postdata['id'],$admin)){
                    $this->session->set_flashdata('alert','Profile has been updated.');
                }
            }
        }else{
            $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');
        }

        redirect(base_url().PATH_TO_ADMIN.'profile');
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect('accounts');
    }
}
