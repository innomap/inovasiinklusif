<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Evaluation_form extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Evaluation Form";
		$this->menu = "evaluation_form";

        $this->load->model('evaluation_form_model');
		$this->load->model('evaluation_focus_model');

		$this->lang->load(PATH_TO_ADMIN.'evaluation_form',$this->language);

		$this->scripts[] = 'administrator/evaluation_form';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['evaluation_form'] = $this->evaluation_form_model->find_all();
        foreach ($data['evaluation_form'] as $key => $value) {
            $data['evaluation_form'][$key]['evaluation_focus'] = $this->evaluation_focus_model->find("evaluation_form_id = ".$value['id']);
        }
    	$data['form_view'] = $this->load->view(PATH_TO_ADMIN.'evaluation_form/form', NULL, TRUE);

		$this->load->view(PATH_TO_ADMIN.'evaluation_form/list', $data);
    }

    function store(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();
        $data_form = array(
            "name" => $postdata['name']);

        if($id = $this->evaluation_form_model->insert($data_form)){
            $this->session->set_flashdata('alert','New Evaluation Form has been created');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluation_form/edit/'.$id);
    }

    public function edit($id = 0){
    	$this->scripts[] = 'administrator/evaluation_focus';

		$evaluation_form = $this->evaluation_form_model->find_by_id($id);
		if($evaluation_form){
			$data['evaluation_form'] = $evaluation_form;
		}

        $data['alert'] = $this->session->flashdata('alert');
        $data['evaluation_focus'] = $this->evaluation_focus_model->find("evaluation_form_id = ".$id);
        $data['form_view'] = $this->load->view(PATH_TO_ADMIN.'evaluation_focus/form', array('form_id' => $id), TRUE);
		
		$this->load->view(PATH_TO_ADMIN.'evaluation_focus/list', $data);
	}

	function update(){
		$this->layout = FALSE;

		$postdata = $this->postdata();
        
    	$data_form = array(
            "name" => $postdata['name']);

        if($this->evaluation_form_model->update($postdata['form_id'], $data_form)){
            $this->session->set_flashdata('alert','New Evaluation Form has been updated');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluation_form');
	}

    function delete($id){
        $this->layout = FALSE;
        if($this->evaluation_form_model->delete($id)){
            $this->session->set_flashdata('alert','Evaluation Form has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Evaluation Form can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluation_form');
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }
}
