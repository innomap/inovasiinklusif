<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Articles extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Article";
		$this->menu = "article";

		$this->load->model('article');

		$this->lang->load(PATH_TO_ADMIN.'article',$this->language);

        $this->scripts[] = 'plugins/summernote/summernote.min';
		$this->scripts[] = 'administrator/article';

        $this->styles[] = 'summernote/summernote';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
        $data['articles'] = $this->article->find_all();

		$this->load->view(PATH_TO_ADMIN.'article/list', $data);
    }

    function add(){
        $data['form_action'] = 'save';

        $this->load->view(PATH_TO_ADMIN.'article/form',$data);
    }

    function save(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();

        if($postdata['title'] != "" && $postdata['menu'] != ""){
            $data = array(
                "menu" => $postdata['menu'],
                "title" => $postdata['title'],
                "content" => $postdata['content'],
                "url" => strtolower(str_replace("&","dan",str_replace(" ", "-", $postdata['title']))),
                "created_at" => date('Y-m-d H:i:s'));

            if($postdata['id'] > 0){
                $id = $this->article->update($postdata['id'],$data);
            }else{
                $id = $this->article->insert($data);
            }
        
            if($id > 0){
                $this->session->set_flashdata('alert','Article has been updated.');
            }else{
                $this->session->set_flashdata('alert','Article has been added.');
            }
        }

        redirect(base_url().PATH_TO_ADMIN.'articles');
    }

    public function edit($id = 0){
        $data['form_action'] = 'save';
        $data['article'] = $this->article->find_one("id = ".$id);
		$this->load->view(PATH_TO_ADMIN.'article/form',$data);
	}

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'articles');
    }

    function delete($id){
        $this->layout = FALSE;
        if($this->article->delete($id)){
            $this->session->set_flashdata('alert','Article has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Article can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'articles');
    }

    function upload_images(){
        $this->layout = FALSE;

        $dir_name = PATH_TO_ARTICLE_IMAGES;
        move_uploaded_file($_FILES['file']['tmp_name'],$dir_name.$_FILES['file']['name']);
        echo $dir_name.$_FILES['file']['name'];
    }
}
