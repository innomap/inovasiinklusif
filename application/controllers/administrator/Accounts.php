<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Accounts extends Common {

	function __construct() {
		parent::__construct("account");

		$this->load->model('user');
		$this->load->model('admin');

		$this->lang->load('account',$this->language);

		$this->scripts[] = 'administrator/account';
    }

    public function index(){
		if ($this->userdata) {
			if($this->userdata['role_id'] == USER_ROLE_SUPER_ADMIN){
				redirect(base_url().PATH_TO_ADMIN.'applications');
			}else if($this->userdata['role_id'] == USER_ROLE_YIM_SUPER_ADMIN){
				redirect(base_url().PATH_TO_ADMIN.'applications');
			}else if($this->userdata['role_id'] == USER_ROLE_DISTRICT_OFFICER){
				redirect(base_url().PATH_TO_ADMIN.'applications');
			}
			
		}else{
			$this->login();
		}
    }

	public function login(){
		$this->title = "Login";
		$data['alert'] = $this->session->flashdata('alert');
		$this->scripts[] = 'site/landing';
		$this->load->view(PATH_TO_ADMIN.'account/form_login',$data);
	}

	function login_auth(){
		$this->layout = FALSE;
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$this->auth($username, $password);
	}

	private function auth($username, $password){
		$response = array();
		
		$user = $this->user->find_one('username = '.$this->db->escape($username).' AND role_id != '.USER_ROLE_INNOVATOR);
		if($user != NULL){
			if($this->check_password($user['username'], $password, $user['password'])){
				$admin = $this->admin->find_one("user_id = ".$user['id']);
				if($admin){
					$data_session = array('id' => $user['id'],'username' => $user['username'],'role_id' => $user['role_id']);

					if($user['role_id'] == USER_ROLE_SUPER_ADMIN){
						$this->user_session->set_admin($data_session);
					}else if($user['role_id'] == USER_ROLE_YIM_SUPER_ADMIN){
						$this->user_session->set_yim_admin($data_session);
					}else if($user['role_id'] == USER_ROLE_DISTRICT_OFFICER){
						$this->user_session->set_district_officer($data_session);
					}
			
					redirect(base_url().PATH_TO_ADMIN.'accounts');
				}else{
					$this->session->set_flashdata('alert','The username or password you entered is incorrect.');
					redirect(base_url().PATH_TO_ADMIN);
				}
			}else{
				$this->session->set_flashdata('alert','The username or password you entered is incorrect.');
				redirect(base_url().PATH_TO_ADMIN);
			}
		}else{
			$this->session->set_flashdata('alert','The username or password you entered is incorrect.');
			redirect(base_url().PATH_TO_ADMIN);
		}
	}

	private function check_password($username, $password, $hash) {
		$password = $this->user->get_hash($username, $password);
		if($password == $hash){
			return true;
		}else{
			return false;
		}
	}

	public function logout() {
		$this->user_session->clear();
		redirect(base_url().PATH_TO_ADMIN.'login');
	}
}
