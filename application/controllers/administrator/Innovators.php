<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Innovators extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Innovator";
		$this->menu = "innovator";

		$this->load->model('innovator');
        $this->load->model('innovator_expert_team');
        $this->load->model('district');
        $this->load->model('user');
        $this->load->model('zone');
        $this->load->model('state');

		$this->lang->load('account',$this->language);

        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
		$this->scripts[] = 'administrator/innovator';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
        $data['innovators'] = $this->innovator->get_join()->result_array();
        $data['application_type'] = unserialize(APPLICATION_TYPE);

		$this->load->view(PATH_TO_ADMIN.'innovator/list', $data);
    }

    public function view($id = 0){
        $data['form_action'] = 'save';
        $data['innovator'] = $this->innovator->get_one_join($id);
        if($data['innovator']){
            $split_ic_num = explode("-", $data['innovator']['ic_number']);
            $data['innovator']['ic_num_1'] = $split_ic_num[0];
            $data['innovator']['ic_num_2'] = $split_ic_num[1];
            $data['innovator']['ic_num_3'] = $split_ic_num[2];
            $data['innovator']['team_experts'] = $this->innovator_expert_team->find("user_id = ".$data['innovator']['user_id']);
            $district = $this->district->find_one("id = ".$data['innovator']['district_id']);
            $data['innovator']['state'] = $this->state->find_one("id = ".$district['state_id']);
        }
        $data['application_types'] = unserialize(APPLICATION_TYPE);
        $data['gender'] = unserialize(GENDER);
        $data['zone'] = $this->zone->find_all();
        $data['states'] = $this->state->find_all();
        $data['districts'] = $this->district->find_all();
        $data['innovation_category'] = unserialize(INNOVATION_CATEGORY);
        $data['referrers'] = unserialize(REFERRERS);
        $data['view_mode'] = 1;

		$this->load->view(PATH_TO_ADMIN.'innovator/form',$data);
	}

    function reset_password($id){
        $data['id'] = $id;

        $this->load->view(PATH_TO_ADMIN.'innovator/form_reset_password',$data);   
    }

    function reset_password_handler(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        if($postdata['password'] != ""){
            $user = $this->user->find_one("id = ".$this->db->escape($postdata['id']));
            if($this->user->update_user($postdata['id'], array('username' => $user['username'],'password' => $postdata['password']))){
                $this->session->set_flashdata('alert','User has been added.');
            }else{
                $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');
            }
        }else{
            $this->session->set_flashdata('alert','The field is required.');
        }

        redirect(base_url().PATH_TO_ADMIN.'innovators');
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'users');
    }

    function delete($id){
        $this->layout = FALSE;
        $user = $this->user->find_by_id($id);

        if($user['requested_to_delete'] == 1){
            if($this->user->delete($id)){
                $this->session->set_flashdata('alert','Innovator has been deleted.');
            }else{
                $this->session->set_flashdata('alert','Innovator can not be deleted.');
            }
        }else{
            if($this->user->update($id, array('requested_to_delete' => 1, 'requested_to_delete_by' => $this->userdata['id']))){
                $this->session->set_flashdata('alert','Request to delete has been sent.');
            }else{
                $this->session->set_flashdata('alert','Innovator can not be deleted.');
            }
        }

        redirect(base_url().PATH_TO_ADMIN.'innovators');
    }

    private function export_to_excel($file_name, $title, $columns, $fields, $contents) {
                $this->load->library('PHPExcel');

                if (!$fields) {
                        echo 'No result. <a href="' . base_url() . PATH_TO_ADMIN . '">Back</a>';
                        die();
                }

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
                header('Cache-Control: max-age=0');

                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();

                // Set properties
                $objPHPExcel->getProperties()->setCreator("Cabaran Inovasi Inklusif");
                $objPHPExcel->getProperties()->setLastModifiedBy("Cabaran Inovasi Inklusif");
                $objPHPExcel->getProperties()->setTitle($title);
                $objPHPExcel->getProperties()->setSubject("Cabaran Inovasi Inklusif");

                #Column Width
                $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);

                #Column Title Style
                $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                #Wrap Text
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true);

                // Add some data
                $objPHPExcel->setActiveSheetIndex(0);
                $col = 0;
                $row = 1;

                foreach ($columns as $column) {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column);
                        $col++;
                }
                $col = 0;
                $row++;
                foreach ($contents as $content) {
                        foreach ($fields as $field) {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $content[$field]);
                                $col++;
                        }
                        $col = 0;
                        $row++;
                }
                // Rename sheet
                $objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

                // Save it as an excel 2003 file
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                //$objWriter->save("nameoffile.xls");
                $objWriter->save('php://output');
                exit;
        }

        private function generate_innovator_data($data){
                $this->lang->load('application',$this->language);

                $application_type = unserialize(APPLICATION_TYPE);
                $gender = unserialize(GENDER);
                $referrers = unserialize(REFERRERS);

                $contents = array();
                $no = 1; 
                foreach ($data as $key => $value) {
                    $contents[$key]['no'] = $no;
                    $contents[$key]['innovator'] = $value['name'];
                    $contents[$key]['application_type'] = $application_type[$value['application_type']];
                    $contents[$key]['ic_number'] = $value['ic_number'];
                    $contents[$key]['gender'] = $gender[$value['gender']];
                    $contents[$key]['address'] = $value['address'];
                    $contents[$key]['postcode'] = $value['postcode'];
                    $contents[$key]['telp_home_no'] = $value['telp_home_no'];
                    $contents[$key]['telp_no'] = $value['telp_no'];
                    $state = $this->state->find_one("id = ".$value['state_id']);
                    $zone = $this->zone->find_one("id = ".$state['zone_id']);
                    $contents[$key]['zone'] = $zone['name'];
                    $contents[$key]['state'] = $state['name'];
                    $contents[$key]['district_name'] = $value['district_name'];
                    $team_member = $this->innovator_expert_team->find("user_id = ".$value['user_id']);
                    $team_member_str = "";
                    foreach ($team_member as $val) {
                        if($val['name'] != ""){
                            $team_member_str = "- ".$val['name']."/".$val['ic_number'];
                        }
                    }
                    $contents[$key]['team_member'] = $team_member_str;
                    $contents[$key]['email'] = $value['email'];
                    $contents[$key]['referrer'] = $referrers[$value['referrer']];
                $no++;}

                return $contents;
        }

        function export_innovator(){
            $this->layout = FALSE;

            $data = $this->innovator->get_join()->result_array();
            $contents = $this->generate_innovator_data($data);

            $file_name = "Innovator_List";
            $title = "Innovator List";
            $columns = array("No",lang('innovator'), lang('application_type'), lang('ic_number'), lang('gender'), lang('address'),lang('postcode'),lang('telp_home_no'), lang('telp_no'), lang('zone'), lang('state'),lang('district'),lang('team_member'), lang('email'), lang('referrers'));
            if(count($contents) > 0){
                $fields = array_keys($contents[0]);

                $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
            }else{
                redirect(base_url().PATH_TO_ADMIN.'innovators');
            }
        } 
}
