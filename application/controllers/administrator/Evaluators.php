<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Evaluators extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Evaluators";
		$this->menu = "evaluators";

		$this->load->model(array('evaluator', 'evaluator_group', 'user'));

		$this->lang->load(PATH_TO_ADMIN.'evaluator',$this->language);

		$this->scripts[] = 'administrator/evaluator';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
        $data['evaluator'] = $this->evaluator->find_all();

        foreach ($data['evaluator'] as $key => $value) {
        	$data['evaluator'][$key]['evaluator_group'] = $this->evaluator_group->find_by_id($value['evaluator_group_id']);
        }

		$this->load->view(PATH_TO_ADMIN.'evaluator/list', $data);
    }

    function add(){
        $data['form_action'] = 'save';
        $data['evaluator_group'] = $this->evaluator_group->find_all();

        $this->load->view(PATH_TO_ADMIN.'evaluator/form',$data);
    }

    function save(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();

        if($postdata['username'] != "" && $postdata['name'] != ""){
            if($this->user->is_username_exist($postdata['username'], $postdata['user_id'])){
                $this->session->set_flashdata('alert','Sorry, your username has been registered.');
            }else{
                $data = array(
                    "username" => $postdata['username'],
                    "status" => USER_STATUS_ACTIVE,
                    "role_id" => 4,
                    "created_at" => date('Y-m-d H:i:s'));
                
                if($postdata['password'] != ""){
                    $data["password"] = $postdata['password'];
                }

                if($postdata['user_id'] > 0){
                    $id = $postdata['user_id'];
                    $this->user->update_user($postdata['user_id'],$data);
                }else{
                    $id = $this->user->insert_user($data);
                }
            
                if($id > 0){
                    $data_evaluator = array(
		                "name" => $postdata['name'],
		                "evaluator_group_id" => $postdata['evaluator_group_id']);

		            if($postdata['id'] > 0){
                        if($this->evaluator->update($postdata['user_id'], $data_evaluator)){
                            $this->session->set_flashdata('alert','Evaluator has been updated.');
                        }
                    }else{
                        $data_evaluator['user_id'] = $id;
                        if($this->evaluator->insert($data_evaluator)){
                            $this->session->set_flashdata('alert','Evaluator has been added.');
                        }
                    }

                }else{
                    $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');
                }
            }
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }

    public function edit($id = 0){
        $data['form_action'] = 'save';
        $data['evaluator'] = $this->evaluator->find_one("id = ".$id);
        $data['evaluator_group'] = $this->evaluator_group->find_all();
        $data['user'] = $this->user->find_by_id($data['evaluator']['user_id']);

        $data['evaluator']['username'] = $data['user']['username'];

		$this->load->view(PATH_TO_ADMIN.'evaluator/form',$data);
	}

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }

    function delete($id){
        $this->layout = FALSE;
        if($this->user->delete($id)){
            $this->session->set_flashdata('alert','Evaluator has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Evaluator can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }
}
