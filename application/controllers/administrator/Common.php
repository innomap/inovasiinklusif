<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	function __construct($module = NULL) {
		parent::__construct();

		$this->layout = ADMIN_LAYOUT;

		$this->language = LANGUAGE_MELAYU;

		$this->menu = array();
		$this->header_menu = array();
		$admin = $this->user_session->get_admin();
		$yim_admin = $this->user_session->get_yim_admin();
		$district_officer = $this->user_session->get_district_officer();
		if($admin){
			$userdata = $admin;
		}else if($yim_admin){
			$userdata = $yim_admin;
		}else if($district_officer){
			$userdata = $district_officer;
		}

		if(isset($userdata)){
			$this->userdata = $userdata;
		}else{
			$this->userdata = array();
		}
		if($module != "account"){
			if (!$this->userdata){
				redirect(base_url().PATH_TO_ADMIN.'login');
			}
		}	
    }

    function generate_pdf($id, $auto_download = TRUE) {
        $this->layout = FALSE;
        $this->load->helper(array('dompdf', 'file'));
        $this->load->model('application');
        $this->load->model('innovator');
        $this->load->model('application_picture');
        $this->load->model('district');
        $this->load->model('zone');
        $this->load->model('state');
        $this->load->model('innovator_expert_team');

        $this->lang->load('application', $this->language);

        $data['application_types'] = unserialize(APPLICATION_TYPE);
        $data['gender'] = unserialize(GENDER);
        $data['innovation_category'] = unserialize(INNOVATION_CATEGORY);
        $data['targets'] = unserialize(INNOVATION_TARGET);
        $data['troubleshootings'] = unserialize(TROUBLESHOOTING_OPTION);
        $data['estimated_market_size'] = unserialize(ESTIMATED_MARKET_SIZE);
        $data['income'] = unserialize(INCOME_OPTION);
        $data['referrers'] = unserialize(REFERRERS);

        if ($data['application'] = $this->application->find_by_id($id)) {
            $data['i_picture'] = $this->application_picture->find("application_id = " . $id);
            $data['i_target'] = ($data['application']['target'] != NULL ? json_decode($data['application']['target']) : array());
            $data['i_troubleshooting'] = ($data['application']['troubleshooting'] != NULL ? json_decode($data['application']['troubleshooting']) : array());

            $data['innovator'] = $this->innovator->get_one_join($data['application']['user_id']);
	        if ($data['innovator']) {
	            $data['innovator']['team_experts'] = $this->innovator_expert_team->find("user_id = " . $data['innovator']['user_id']);
	            $district = $this->district->find_one("id = ".$data['innovator']['district_id']);
	            $state = $this->state->find_one("id = ".$district['state_id']);
	            $zone = $this->zone->find_one("id = ".$state['zone_id']);
	            $data['innovator']['district'] = $district['name'];
	            $data['innovator']['state'] = $state['name'];
	            $data['innovator']['zone'] = $zone['name'];
	        }
        }

        $filename = $id . "_" . str_replace(" ", "_", $data['application']['title']) . ".pdf";
        
        $this->application->update($id, array(
            "pdf" => $filename,
        ));
        
        $html = $this->load->view('application/form_pdf', $data, true);
        
        generate_pdf($html, $filename, $auto_download);                
	}

	function generate_html($id, $auto_download = TRUE) {
        $this->layout = FALSE;
        $this->load->helper(array('dompdf', 'file'));
        $this->load->model('application');
        $this->load->model('innovator');
        $this->load->model('application_picture');
        $this->load->model('district');
        $this->load->model('zone');
        $this->load->model('state');
        $this->load->model('innovator_expert_team');

        $this->lang->load('application', $this->language);

        $data['application_types'] = unserialize(APPLICATION_TYPE);
        $data['gender'] = unserialize(GENDER);
        $data['innovation_category'] = unserialize(INNOVATION_CATEGORY);
        $data['targets'] = unserialize(INNOVATION_TARGET);
        $data['troubleshootings'] = unserialize(TROUBLESHOOTING_OPTION);
        $data['estimated_market_size'] = unserialize(ESTIMATED_MARKET_SIZE);
        $data['income'] = unserialize(INCOME_OPTION);
        $data['referrers'] = unserialize(REFERRERS);

        if ($data['application'] = $this->application->find_by_id($id)) {
            $data['i_picture'] = $this->application_picture->find("application_id = " . $id);
            $data['i_target'] = ($data['application']['target'] != NULL ? json_decode($data['application']['target']) : array());
            $data['i_troubleshooting'] = ($data['application']['troubleshooting'] != NULL ? json_decode($data['application']['troubleshooting']) : array());

            $data['innovator'] = $this->innovator->get_one_join($data['application']['user_id']);
	        if ($data['innovator']) {
	            $data['innovator']['team_experts'] = $this->innovator_expert_team->find("user_id = " . $data['innovator']['user_id']);
	            $district = $this->district->find_one("id = ".$data['innovator']['district_id']);
	            $state = $this->state->find_one("id = ".$district['state_id']);
	            $zone = $this->zone->find_one("id = ".$state['zone_id']);
	            $data['innovator']['district'] = $district['name'];
	            $data['innovator']['state'] = $state['name'];
	            $data['innovator']['zone'] = $zone['name'];
	        }
        }

        $filename = $id . "_" . str_replace(" ", "_", $data['application']['title']) . ".pdf";
        
        $this->application->update($id, array(
            "pdf" => $filename,
        ));
        
        $html = $this->load->view('application/form_pdf', $data, true);
        echo $html;
        //generate_pdf($html, $filename, $auto_download);                
	}
}