<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Site extends Common {

	function __construct() {
		parent::__construct("site");

		$this->layout = DEFAULT_LAYOUT;
		$this->load->model('user_session');
        $this->load->model('article');

		$this->title = "Site";
    }

    public function index(){
    }

    function article($url){
        $data['article'] = $this->article->find_one("url = '".$url."'");
        $this->load->view('site/article',$data);
    }
}
