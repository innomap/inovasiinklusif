<?php

function ellipsis($str,$length){
	return strlen($str) > $length ? substr($str,0,$length)."..." : $str;
}