<div class="col-md-12">
	<?php $this->load->view('partial/logo') ?>
    
    <div class="col-md-12 content-box">
    	<div class="col-md-12 text-center">
    		<h2><?= lang('innovator_locator') ?></h2>
    	</div>
		<div id="map-canvas col-md-12" style="margin: 10px auto 20px;">	
			<?php echo $map['html']; ?>
			<div style="display:none"><?php print_r($map); ?></div>
		</div>
    </div>
</div>