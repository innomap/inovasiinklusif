<div class="col-md-12 report_type" data-content="submission-num-by-date">
        <div class="row">
                <form id="form-submission-num" action="<?= base_url() . PATH_TO_ADMIN . 'reports/export_submission_number_by_date' ?>" method="post">
                        <div class="form-group col-md-12">
                                <div class="col-md-1">
                                        <label><?= lang('filter') ?>:</label>
                                </div>
                                <div class="col-md-2">
                                        <label><?= lang('from') ?></label>
                                        <input readonly type="text" class="form-control from-datepicker" name="date_from" value="<?= $from_date ?>">
                                </div>
                                <div class="col-md-2">
                                        <label><?= lang('to') ?></label>
                                        <input readonly type="text" class="form-control to-datepicker" name="date_to" value="<?= $to_date ?>">
                                </div>
                                <div class="col-md-3 report-filter-act">
                                        <button type="button" class="btn btn-primary flat" id="submit-submission-num">Hantar</button>
                                        <button type="submit" class="btn btn-success flat" id="export-submission-num">Export to Excel</button>
                                </div>
                        </div>
                </form>
        </div>

        <div class="col-md-12" id="container-submission-num-by-date">
        </div>
</div>
<style>
        .form-control {
                cursor: text !important;
                background-color: white !important;
        }
</style>
<?= $modal_detail; ?>