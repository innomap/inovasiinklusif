<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<form role="form" id="form-detail" action="<?= site_url(PATH_TO_ADMIN.'reports/'.$action_url) ?>" method="POST">
			<input type="hidden" name="date">
			<input type="hidden" name="category">
			<input type="hidden" name="subcategory">
			<input type="hidden" name="zone_id">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Detail</h4>
				</div>
				
				<div class="modal-body">
				</div>
				
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary btn-export" value="Export to Excel"/>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>