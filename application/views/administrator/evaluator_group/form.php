<script type="text/javascript">
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_group = "<?= lang('delete_group') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>",
		edit_mode = "<?= (isset($group) ? 1 : 0) ?>";
</script>
<?php $this->load->view('partial/logo') ?>
<div class="col-md-8 col-md-offset-2 content-box bg-light-grey">
	<div class="col-md-12 text-center">
		<h2><?= lang('form_group') ?></h2>
    </div>
	<form role="form" class="form-horizontal" id="form-group" action="<?= site_url(PATH_TO_ADMIN.'evaluator_groups/'.$form_action.'/') ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= isset($group) ? $group['id'] : '' ?>" />
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('name') ?></label>
				<div class="col-md-9">
					<input type="text" name="name" class="form-control" placeholder="<?= lang('name') ?>" value="<?= isset($group) ? $group['name'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('description') ?></label>
				<div class="col-md-9">
					<textarea name="description" rows="6" class="form-control" placeholder="<?= lang('description') ?>"><?= isset($group) ? $group['description'] : '' ?></textarea>
				</div>
			</div>
			
			<div class="form-group text-center">
				<button type="button" class="btn btn-grey btn-cancel"><?= lang('cancel') ?></button>
				<button type="submit" class="btn btn-dark-turquoise flat"><?= lang('save') ?></button>
			</div>
		</form>
</div>