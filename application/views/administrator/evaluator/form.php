<script type="text/javascript">
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_group = "<?= lang('delete_group') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>",
		edit_mode = "<?= (isset($evaluator) ? 1 : 0) ?>";
</script>
<?php $this->load->view('partial/logo') ?>
<div class="col-md-8 col-md-offset-2 content-box bg-light-grey">
	<div class="col-md-12 text-center">
		<h2><?= lang('form_evaluator') ?></h2>
    </div>
	<form role="form" class="form-horizontal" id="form-evaluator" action="<?= site_url(PATH_TO_ADMIN.'evaluators/'.$form_action.'/') ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= isset($evaluator) ? $evaluator['id'] : '' ?>" />
		<input type="hidden" name="user_id" value="<?= isset($evaluator) ? $evaluator['user_id'] : '' ?>" />
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('username') ?></label>
				<div class="col-md-9">
					<input type="text" name="username" class="form-control" placeholder="<?= lang('username') ?>" value="<?= isset($evaluator) ? $evaluator['username'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="button" class="btn btn-default hide btn-change-pass"><?= lang('change_password') ?></button>
				</div>
			</div>

			<div class="collapse-group">
				<div class="form-group pass-group">
					<label class="col-md-3 control-label"><?= lang('password') ?></label>
					<div class="col-md-9">
						<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
					</div>
				</div>

				<div class="form-group pass-group">
					<label class="col-md-3 control-label"><?= lang('retype_password') ?></label>
					<div class="col-md-9">
						<input type="password" class="form-control" name="retype_password" placeholder="<?= lang('retype_password') ?>">	
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('name') ?></label>
				<div class="col-md-9">
					<input type="text" name="name" class="form-control" placeholder="<?= lang('name') ?>" value="<?= isset($evaluator) ? $evaluator['name'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('group') ?></label>
				<div class="col-md-9">
					<select class="form-control" name="evaluator_group_id">
						<option value="">-- <?= lang('select_group') ?> --</option>
						<?php foreach ($evaluator_group as $key => $value) { ?>
							<option value="<?= $value['id'] ?>" <?= (isset($evaluator) ? ($evaluator['evaluator_group_id'] == $value['id'] ? 'selected' : '') : '') ?>><?= $value['name'] ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			
			<div class="form-evaluator text-center">
				<button type="button" class="btn btn-grey btn-cancel"><?= lang('cancel') ?></button>
				<button type="submit" class="btn btn-dark-turquoise flat"><?= lang('save') ?></button>
			</div>
		</form>
</div>