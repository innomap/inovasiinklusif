<script type="text/javascript">
	var alert = "<?= $alert ?>";
	var lang_announcement = '<?= lang("landing_announcement") ?>';
</script>

<div class="col-md-12 col-sm-12 col-xs-12 container margin-top-50">
	<div class="col-md-4 col-sm-12 col-xs-12 login-box bg-white-trans">
		<div class="col-md-12 text-center">
			<h2>Administrator</h2>
		</div>

		<div class="col-md-12 margin-top-30">
			<div class="alert alert-info alert-dismissable hide">
			    <i class="fa fa-info"></i>
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			    <?= $alert ?>
			</div>
			<form id="form-login" action="<?= base_url().PATH_TO_ADMIN.'accounts/login_auth' ?>" method="POST">
				<div class="form-group">
					<input type="text" class="form-control" name="username" placeholder="<?= lang('username') ?>">
				</div>

				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
				</div>

				<div class="col-md-12 text-right">
					<input type="submit" class="btn btn-black" name="btn_login" value="Masuk">
				</div>
			</form>
		</div>
	</div>
</div>