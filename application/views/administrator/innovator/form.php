<script type="text/javascript">
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_user = "<?= lang('delete_user') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>",
		edit_mode = "<?= (isset($user) ? 1 : 0) ?>";
</script>
<?php $this->load->view('partial/logo') ?>

<div class="col-md-8 col-md-offset-2 content-box bg-light-grey">
	<div class="col-md-12 text-center">
		<h2><?= lang('form_innovator') ?></h2>
    </div>
	<form role="form" class="form-horizontal" id="form-user" action="<?= site_url(PATH_TO_ADMIN.'users/'.$form_action.'/') ?>" method="POST">
		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('application_type') ?></label>
			<div class="col-md-9">
				<?php foreach ($application_types as $key => $value) { ?>
					<div class="radio"> 
						<label> <input type="radio" name="application_type" value="<?= $key ?>" <?= (isset($innovator) ? $key == $innovator['application_type'] ? 'checked' : '' : $key == 0 ? 'checked' : '') ?> disabled> <?= $value ?> </label> 
					</div>
				<?php } ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('name') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="name" placeholder="<?= lang('name') ?>" value="<?= (isset($innovator) ? $innovator['name'] : '') ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('ic_number') ?></label>
			<div class="col-md-9">
				<div class="input-group">
					<input type="text" class="form-control" name="ic_num_1" placeholder="<?= lang('ic_number') ?>" value="<?= (isset($innovator) ? $innovator['ic_num_1'] : '') ?>" readonly>
					<span class="input-group-addon">-</span>
					<input type="text" class="form-control" name="ic_num_2" value="<?= (isset($innovator) ? $innovator['ic_num_2'] : '') ?>" readonly>
					<span class="input-group-addon">-</span>
					<input type="text" class="form-control" name="ic_num_3" value="<?= (isset($innovator) ? $innovator['ic_num_3'] : '') ?>" readonly>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('gender') ?></label>
			<div class="col-md-9">
				<?php foreach ($gender as $key => $value) { ?>
					<div class="radio"> 
						<label> <input type="radio" name="gender" value="<?= $key ?>" <?= (isset($innovator) ? $key == $innovator['gender'] ? 'checked' : '' : $key == 0 ? 'checked' : '') ?> disabled> <?= $value ?> </label> 
					</div>
				<?php } ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('address') ?></label>
			<div class="col-md-9">
				<textarea class="form-control" name="address" rows="5" placeholder="<?= lang('address') ?>" disabled><?= (isset($innovator) ? $innovator['address'] : '') ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('postcode') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="postcode" placeholder="<?= lang('postcode') ?>" value="<?= (isset($innovator) ? $innovator['postcode'] : '') ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('zone') ?></label>
			<div class="col-md-9">
				<select class="form-control" name="zone_id" disabled>
					<option value="">-- <?= lang('select_zone') ?> --</option>
					<?php foreach ($zone as $key => $value) { ?>
						<option value="<?= $value['id'] ?>" <?= (isset($innovator) ? $value['id'] == $innovator['state']['zone_id'] ? 'selected' : '' : '') ?>><?= $value['name'] ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('state') ?></label>
			<div class="col-md-9">
				<select class="form-control" name="state_id" disabled>
					<option value="">-- <?= lang('select_state') ?> --</option>
					<?php foreach ($states as $key => $value) { ?>
						<option value="<?= $value['id'] ?>" <?= (isset($innovator) ? $value['id'] == $innovator['state']['id'] ? 'selected' : '' : '') ?>><?= $value['name'] ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('district') ?></label>
			<div class="col-md-9">
				<select class="form-control" name="district_id" disabled>
					<option value="">-- <?= lang('select_district') ?> --</option>
					<?php foreach ($districts as $key => $value) { ?>
						<option value="<?= $value['id'] ?>" <?= (isset($innovator) ? $value['id'] == $innovator['district_id'] ? 'selected' : '' : '') ?>><?= $value['name'] ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('telp_home_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="telp_home_no" placeholder="<?= lang('telp_home_no') ?>" value="<?= (isset($innovator) ? $innovator['telp_home_no'] : '') ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="telp_no" placeholder="<?= lang('telp_no') ?>" value="<?= (isset($innovator) ? $innovator['telp_no'] : '') ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('photo') ?></label>
			<div class="col-md-9">
				<?php if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $innovator['photo']) && $innovator['photo'] != "") { ?>
						<a href="<?= base_url().PATH_TO_INNOVATOR_PHOTO.$innovator['photo'] ?>" class="fancyboxs"><img src="<?= base_url().PATH_TO_INNOVATOR_PHOTO_THUMB.$innovator['photo'] ?>" width="30%"/></a>
				<?php } ?>
			</div>
		</div>			

		<?php if($innovator['application_type'] != APPLICATION_TYPE_INDIVIDU){ ?>
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('team_member') ?></label>
				<div class="col-md-9">
					<?php foreach ($innovator['team_experts'] as $key => $value) { ?>
						<div class="col-md-12 team-member-item no-padding">
							<div class="col-md-12 no-padding">
								<div class="col-md-4">
									<?php if(file_exists(realpath(APPPATH . '../'.PATH_TO_TEAM_MEMBER_PHOTO) . DIRECTORY_SEPARATOR . $value['photo']) && $value['photo'] != "") { ?>
											<a href="<?= base_url().PATH_TO_TEAM_MEMBER_PHOTO.$value['photo'] ?>" class="fancyboxs"><img src="<?= base_url().PATH_TO_TEAM_MEMBER_PHOTO_THUMB.$value['photo'] ?>" width="100%"/></a>
									<?php } ?>
								</div>
								<div class="col-md-4 no-padding-left">
									<input type="text"class="form-control" name="team_member_0" placeholder="<?= lang('name') ?>" value="<?= $value['name'] ?>" readonly>
								</div>
								<div class="col-md-4 no-padding-left">
									<input type="text"class="form-control" name="team_member_ic_num_0" placeholder="<?= lang('ic_number') ?>" value="<?= $value['ic_number'] ?>" readonly>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		<?php } ?>

		<div class="form-group">
            <label class="col-md-3 control-label"><?= lang('email') ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>" value="<?= (isset($innovator) ? $innovator['email'] : '') ?>" readonly>
            </div>
        </div>

        <div class="form-group">
			<label class="col-md-3 control-label"><?= lang('referrers') ?></label>
			<div class="col-md-9">
				<select class="form-control" name="referrer" disabled>
					<?php foreach ($referrers as $key => $value) { ?>
						<option value="<?= $key ?>" <?= (isset($innovator) ? $key == $innovator['referrer'] ? 'selected' : '' : '') ?>><?= $value ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

			
		<div class="form-group text-center">
			<?php if(!isset($view_mode)){ ?>
				<button type="button" class="btn btn-grey btn-cancel"><?= lang('cancel') ?></button>
				<button type="submit" class="btn btn-dark-turquoise flat"><?= lang('save') ?></button>
			<?php }else{ ?>
				<button type="button" class="btn btn-grey btn-back"><?= lang('back') ?></button>
			<?php } ?>
		</div>
	</form>
</div>