<?php $this->load->view('partial/logo') ?>

<div class="col-md-8 col-md-offset-2 content-box bg-light-grey">
	<div class="col-md-12 text-center">
		<h2><?= lang('reset_password') ?></h2>
    </div>
	<form role="form" class="form-horizontal" id="form-reset-password" action="<?= site_url(PATH_TO_ADMIN.'innovators/reset_password_handler') ?>" method="POST">
		<input type="hidden" name="id" value="<?= $id ?>">
		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('password') ?></label>
			<div class="col-md-9">
				<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('retype_password') ?></label>
			<div class="col-md-9">
				<input type="password" class="form-control" name="retype_password" placeholder="<?= lang('retype_password') ?>">	
			</div>
		</div>
			
		<div class="form-group text-center">
			<button type="button" class="btn btn-grey btn-cancel"><?= lang('cancel') ?></button>
			<button type="submit" class="btn btn-dark-turquoise flat"><?= lang('save') ?></button>
		</div>
	</form>
</div>