<script type="text/javascript">
    var alert = "<?= $alert ?>",
    userId = "<?= $this->userdata['id'] ?>";
</script>
<?php $this->load->view('partial/logo') ?>

<div class="col-md-8 col-md-offset-2">
        <div class="col-md-12 text-center margin-btm-20">
                <h2><?= lang('edit_profile') ?></h2>
        </div>

        <div class="col-md-12 alert alert-info hide alert-dismissable">
                <i class="fa fa-info"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?= $alert ?>
        </div>

        <form id="form-profile" class="form-horizontal" action="<?= base_url() . 'profile/save' ?>" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $innovator['user_id'] ?>">
                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('username') ?></label>
                        <div class="col-md-9">
                                <input type="text" disabled class="form-control" name="username" placeholder="<?= lang('username') ?>" value="<?= $innovator['username'] ?>">
                        </div>
                </div>

                <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-default btn-change-pass">Ubah Kata Laluan</button>
                        </div>
                </div>
                <div class="collapse-group collapse-password collapse">
                        <div class="form-group">
                                <label class="col-md-3 control-label"><?= lang('new_password') ?></label>
                                <div class="col-md-9">
                                        <input type="password" name="password" placeholder="<?= lang('new_password') ?>" class="form-control">
                                </div>
                        </div>

                        <div class="form-group">
                                <label class="col-md-3 control-label"><?= lang('retype_new_password') ?></label>
                                <div class="col-md-9">
                                        <input type="password" name="retype_password" placeholder="<?= lang('retype_new_password') ?>" class="form-control">
                                </div>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('application_type') ?></label>
                        <div class="col-md-9">
                                <?php foreach ($application_types as $key => $value) { ?>
                                        <div class="radio"> 
                                                <label> <input type="radio" name="application_type" value="<?= $key ?>" <?= ($innovator['application_type'] == $key ? 'checked' : '') ?>> <?= $value ?> </label> 
                                        </div>
                                <?php } ?>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('name') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control" name="name" placeholder="<?= lang('name') ?>" value="<?= $innovator['name'] ?>">
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('ic_number') ?></label>
                        <div class="col-md-9">
                                <div class="input-group">
                                        <input type="text" class="form-control number-only" maxlength="6" name="ic_num_1" placeholder="<?= lang('ic_number') ?>" value="<?= $innovator['ic_num_1'] ?>">
                                        <span class="input-group-addon">-</span>
                                        <input type="text" class="form-control number-only" maxlength="2" name="ic_num_2" value="<?= $innovator['ic_num_2'] ?>">
                                        <span class="input-group-addon">-</span>
                                        <input type="text" class="form-control number-only" maxlength="4" name="ic_num_3" value="<?= $innovator['ic_num_3'] ?>">
                                </div>
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label"><?= lang('email') ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="email" placeholder="Contoh: xyz@gmail.com" value="<?= $innovator['email'] ?>">
                    </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('gender') ?></label>
                        <div class="col-md-9">
                                <?php foreach ($gender as $key => $value) { ?>
                                        <div class="radio"> 
                                                <label> <input type="radio" name="gender" value="<?= $key ?>" <?= $key == $innovator['gender'] ? 'checked' : '' ?>> <?= $value ?> </label> 
                                        </div>
                                <?php } ?>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('address') ?></label>
                        <div class="col-md-9">
                                <textarea class="form-control" name="address" rows="5" placeholder="<?= lang('address') ?>"><?= $innovator['address'] ?></textarea>
        <!--				<input type="hidden" name="latitude" value="<? $innovator['latitude'] ?>">
                                <input type="hidden" name="longitude" value="<? $innovator['longitude'] ?>">-->

                                <input type="hidden" name="latitude" value="">
                                <input type="hidden" name="longitude" value="">
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('postcode') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control" name="postcode" placeholder="<?= lang('postcode') ?>" value="<?= $innovator['postcode'] ?>">
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('zone') ?></label>
                        <div class="col-md-9">
                            <select class="form-control" name="zone_id" id="zone-id">
                                <option value="">-- <?= lang('select_zone') ?> --</option>
                                <?php foreach ($zone as $key => $value) { ?>
                                        <option value="<?= $value['id'] ?>" <?= $innovator['state']['zone_id'] == $value['id'] ? 'selected' : '' ?>><?= $value['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('state') ?></label>
                        <div class="col-md-9">
                            <select class="form-control state-wrap" name="state_id" id="state-id">
                                <option value="">-- <?= lang('select_state') ?> --</option>
                                <?php foreach ($states as $key => $value) { ?>
                                        <option value="<?= $value['id'] ?>" <?= $innovator['state']['id'] == $value['id'] ? 'selected' : '' ?>><?= $value['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('district') ?></label>
                        <div class="col-md-9">
                                <select class="form-control district-wrap" name="district_id">
                                        <option value="">-- <?= lang('select_district') ?> --</option>
                                        <?php foreach ($districts as $key => $value) { ?>
                                                <option value="<?= $value['id'] ?>" <?= ($value['id'] == $innovator['district_id'] ? 'selected' : '') ?>><?= $value['name'] ?></option>
                                        <?php } ?>
                                </select>
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control number-only" minlength="10" maxlength="11" name="telp_no" placeholder="<?= lang('telp_no') ?>" value="<?= $innovator['telp_no'] ?>">
                        </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label"><?= lang('telp_home_no') ?></label>
                        <div class="col-md-9">
                                <input type="text" class="form-control number-only" minlength="9" maxlength="10" name="telp_home_no" placeholder="<?= lang('telp_home_no') ?>" value="<?= $innovator['telp_home_no'] ?>">
                        </div>
                </div>
                
                <div class="form-group file-field">
                        <label class="col-md-3 control-label"><?= lang('photo') ?></label>
                        <div class="col-md-9">
                                <?php if (file_exists(realpath(APPPATH . '../' . PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $innovator['photo']) && $innovator['photo'] != "") { ?>
                                        <a href="<?= base_url() . PATH_TO_INNOVATOR_PHOTO . $innovator['photo'] ?>" class="fancyboxs"><img src="<?= base_url() . PATH_TO_INNOVATOR_PHOTO_THUMB . $innovator['photo'] ?>" width="30%"/></a>
                                <?php } ?>
                                <input type="file" class="form-control" name="innovator_photo">
                        </div>
                </div>				

                <div class="form-group collapse-team-member collapse <?= ($innovator['application_type'] %2 != APPLICATION_TYPE_INDIVIDU ? 'in' : '') ?>">
                        <label class="col-md-3 control-label"><?= lang('team_member') ?></label>
                        <div class="col-md-9 team-member-wrap">
                                <?php
                                if ($innovator['application_type'] %2 != APPLICATION_TYPE_INDIVIDU) {
                                        foreach ($innovator['team_experts'] as $key => $value) {
                                                if (file_exists(realpath(APPPATH . '../' . PATH_TO_TEAM_MEMBER_PHOTO_THUMB) . DIRECTORY_SEPARATOR . $value['photo'])) {
                                                        $url = site_url() . PATH_TO_TEAM_MEMBER_PHOTO_THUMB . $value['photo'];
                                                } else {
                                                        $url = "";
                                                }
                                                ?>
                                                <input type="hidden" class="team-member-id" name="team_member_id[]" value="<?= $value['id'] ?>">
                                                <div class="col-md-12 team-member-item no-padding margin-top-10">
                                                        <input type="hidden" name="h_team_member_pic_<?= $key ?>" value="<?= $value['photo'] ?>">
                                                        <input type="hidden" name="i_deleted_team_member_<?= $key ?>">

                                                        <div class="col-md-11 no-padding">
                                                                <div class="col-md-6 no-padding-left">
                                                                        <input type="text"class="form-control" name="team_member_<?= $key ?>" placeholder="<?= lang('name') ?>" value="<?= $value['name'] ?>">
                                                                </div>
                                                                <div class="col-md-6 no-padding-left">
                                                                        <input type="text"class="form-control" name="team_member_ic_num_<?= $key ?>" placeholder="<?= lang('ic_number') ?>" value="<?= $value['ic_number'] ?>">
                                                                </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                                <?php if ($key == 0) { ?>
                                                                        <button type="button" class="btn btn-default add-team-member"><span class="fa fa-plus"></span></button>
                                                                <?php } else { ?>
                                                                        <button type="button" class="btn btn-danger delete-team-member" data-id="<?= $value['id'] ?>"><span class="fa fa-times"></span></button>
                                                                <?php } ?>
                                                        </div>
                                                        <div class="col-md-12 no-padding-left padding-top-10">
                                                                <div class="col-md-3">
                                                                        <label><?= lang('photo_team') ?></label>
                                                                </div>
                                                                <?php if ($url != "") { ?>
                                                                        <div class="col-md-4">
                                                                                <a href="<?= $url ?>" class="fancyboxs">
                                                                                        <img src="<?= $url ?>" width="100%">
                                                                                </a>
                                                                        </div>
                                                                <?php } ?>
                                                                <div class="col-md-<?= $url != "" ? '5' : '9' ?> no-padding">
                                                                        <input type="file" class="form-control" name="team_member_pic_<?= $key ?>">
                                                                </div>
                                                        </div>
                                                </div>
                                        <?php }
                                } else {
                                        ?>
                                        <input type="hidden" class="team-member-id" name="team_member_id[]" value="0">
                                        <div class="col-md-12 team-member-item no-padding">
                                                <input type="hidden" name="h_team_member_pic_0" value="">
                                                <input type="hidden" name="i_deleted_team_member_0">

                                                <div class="col-md-11 no-padding">
                                                        <div class="col-md-6 no-padding-left">
                                                                <input type="text"class="form-control" name="team_member_0" placeholder="<?= lang('name') ?>">
                                                        </div>
                                                        <div class="col-md-6 no-padding-left">
                                                                <input type="text"class="form-control" name="team_member_ic_num_0" placeholder="<?= lang('ic_number') ?>">
                                                        </div>
                                                </div>
                                                <div class="col-md-1">
                                                        <button type="button" class="btn btn-default add-team-member"><span class="fa fa-plus"></span></button>
                                                </div>
                                                <div class="col-md-12 no-padding-left padding-top-10">
                                                        <div class="col-md-3">
                                                                <label><?= lang('photo_team') ?></label>
                                                        </div>
                                                        <div class="col-md-9 no-padding">
                                                                <input type="file" class="form-control" name="team_member_pic_0">
                                                        </div>
                                                </div>
                                        </div>
        <?php } ?>
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label"><?= lang('referrers') ?></label>
                    <div class="col-md-9">
                        <select class="form-control" name="referrer">
                            <?php foreach ($referrers as $key => $value) { ?>
                                    <option value="<?= $key ?>" <?= $innovator['referrer'] == $key ? 'selected' : '' ?>><?= $value ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-12 text-center">
                        <input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-dark-turquoise" value="<?= lang('save') ?>">
                </div>
        </form>
</div>