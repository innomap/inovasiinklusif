<script>
var lang_announcement = '<?= lang("landing_announcement") ?> <br/> <br/> <p class="text-center text-red"><b>PENDAFTARAN DAN PENYERTAAN ADALAH PERCUMA!<b></p> <br/> <p class="text-center"><b>MAKLUMAN : PERUBAHAN TARIKH & TEMPAT PERTANDINGAN ZON SABAH DAN SARAWAK</b></p> <br/> <img src="<?= base_url().ASSETS_IMG ?>schedule.png" width="100%">';
</script>
<div class="col-md-12 col-sm-12 col-xs-12 container no-padding margin-top-50">
	<div class="col-md-10 col-sm-12 col-xs-12 no-padding col-md-offset-1">
		<div class="col-md-8 col-sm-12 col-xs-12 no-padding-left">
			<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zM3kl3e0x9U" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-30">
				<?php foreach ($district_zone as $key => $value) { ?>
					<div class="col-md-6 col-sm-12 col-xs-12 no-padding-left landing-thumbnail">
						<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
							<div class="col-md-12 col-sm-12 col-xs-12 zone-item bg-white">
								<div class="col-md-6 col-sm-6 col-xs-6 text-right">
									<img src="<?= base_url().ASSETS_IMG.'logo-cabaran-inklusif.png' ?>" width="100%">
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 no-padding zone-item-text">
									<h3 class="no-margin">ZON</h3>
									<h3 class="no-margin"><b><?= $value['name'] ?></b></h3>
									<h5 class="no-margin small">
										<?php foreach ($value['states'] as $state) {
											echo $state['name'].", ";
										} ?>
									</h5>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12 no-padding-right margin-top-30 small">
									<?= $value['description']; ?>
								</div>

								
								<div class="col-md-12 col-sm-12 col-xs-12 margin-top-30 text-right small">
									<a <?= strtotime($value['deadline']) >= strtotime(date('Y-m-d')) ? "href='".base_url()."register'" : "" ?> target="_blank" class="link-black <?= strtotime($value['deadline']) >= strtotime(date('Y-m-d')) ? "" : "disabled" ?>"><u>Daftar Disini</u></a>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>

		<div class="col-md-4 col-sm-12 col-xs-12 no-padding-left text-center">
			<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/t8YDLius3fg" frameborder="0" allowfullscreen></iframe>
				</div>

				<div class="embed-responsive embed-responsive-16by9 margin-top-10">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BuSFBWlsWhk" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-30">
				<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
					<div class="col-md-12 col-sm-12 col-xs-12 border-dot bg-white">
						<h4><b>TENTANG CABARAN</b></h4><br>
						<p class="small"><b>Yayasan Inovasi Malaysia (YIM) melalui anak syarikat YIM
						Technology Resources Sdn Bhd (YTR) telah dilantik sebagai
						agensi peneraju dan pelaksana Program Berimpak Tinggi 6
						– Inovasi Inklusif (HIP 6) di bawah SME Masterplan semasa
						Mesyuarat Majlis Pembangunan PKS Kebangsaan Kali Ke-16.
						Melalui program ini, YTR mengambil inisiatif untuk
						menganjurkan Pertandingan Cabaran Inovasi Inklusif (I²) 2016
						di setiap Zon Seluruh Malaysia setelah melihat keberkesanan
						dalam menjayakan program Cabaran Inovasi Inklusif 2015
						untuk Zon Tengah (Selangor, Kuala Lumpur dan Putrajaya)
						pada tahun 2015.</b></p>
						<p class="small"><b>Pertandingan ini diadakan bertujuan untuk memberi peluang
						kepada rakyat Malaysia yang mempunyai idea kreatif dan
						inovatif dalam mencari inovasi ìnklusif yang mampu
						menyelesaikan masalah (dari segi kepenggunaan, kesihatan,
						produktiviti dan pendidikan) yang sering dihadapi penduduk
						sekitar negeri masing-masing di seluruh Malaysia.<br/><br/><br/><br/><br/><br/></b></p>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<div class="col-md-3 col-md-offset-8 col-sm-12 col-xs-12 no-padding text-right margin-top-30">
		<div class="fb-like" data-href="https://www.facebook.com/cabaraninovasiinklusif" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true" data-width="250px"></div>
	</div>
</div>