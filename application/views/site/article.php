<div class="col-md-12 col-sm-12 col-xs-12 container margin-top-50">
	<div class="col-md-7 col-sm-12 col-xs-12">
		<div class="col-md-12">
			<div class="col-md-12 text-left margin-btm-20">
				<div class="article-title"><?= $article['title'] ?></div>
			</div>
			<div class="col-md-12 padding-btm-20 article-content">
				<?= $article['content'] ?>
			</div>
		</div>
	</div>
</div>


