<?php
	$ol_user = $this->user_session->get_user();
	$ol_evaluator = $this->user_session->get_evaluator();
	$ol_admin = $this->user_session->get_admin();
?>
<div class="col-md-12">
	<div class="col-md-6 col-md-offset-3">
		<div class="col-md-2 col-sm-2 col-xs-2">
			<a href="http://www.miti.gov.my/" target="_blank"><img src="<?= base_url().ASSETS_IMG.'logo-miti.png' ?>" width="80%"/></a>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-3">
			<a href="http://www.mosti.gov.my/" target="_blank"><img src="<?= base_url().ASSETS_IMG.'logo-mosti.png' ?>" width="100%"/></a>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-3">
			<a href="http://www.smecorp.gov.my/index.php/en/" target="_blank"><img src="<?= base_url().ASSETS_IMG.'logo-sme.png' ?>" width="100%"/></a>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2">
			<a href="http://www.yim.my/" target="_blank"><img src="<?= base_url().ASSETS_IMG.'logo-yim.png' ?>" width="80%"/></a>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2">
			<a href="http://www.ytr.my/" target="_blank"><img src="<?= base_url().ASSETS_IMG.'logo-ytr.png' ?>" width="80%"/></a>
		</div>
	</div>
	<?php if(!$ol_user && !$ol_evaluator && !$ol_admin){ ?>
	<div class="col-md-12 footer-navbar">
		<nav class="navbar navbar-basic text-center">
            <div>
                <ul class="nav navbar-nav">
                    <?php foreach ($this->header_menu as $key => $value) { ?>
                        <li class="<?= $this->uri->segment(3) == $value['url'] ? 'active' : '' ?>"><a href="<?= base_url().'site/article/'.$value['url'] ?>"><?= $value['menu'] ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </nav>
	</div>
	<?php } ?>
	<div class="col-md-12 footer-text">
		Copyright &copy 2016 YIM Technology Resources Sdn Bhd. All Rights Reserved.
	</div>
</div>