<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>
<div class="col-md-12 container margin-top-50">
	<div class="col-md-4 login-box bg-white-trans">
		<div class="col-md-12">
			<div class="alert alert-info alert-dismissable hide">
			    <i class="fa fa-info"></i>
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			    <?= $alert ?>
			</div>

			<form id="form-login" action="<?= base_url().'accounts/login_auth' ?>" method="POST">
				<div class="form-group">
					<input type="text" class="form-control" name="username" placeholder="<?= lang('username') ?>">
				</div>

				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
				</div>

				<div class="form-group text-right">
					<input type="submit" class="btn btn-black" name="btn_login" value="Masuk">
				</div>
			</form>
		</div>
		<?php //date_default_timezone_set("Asia/Kuala_Lumpur");
            //if(strtotime("2016-07-15 16:59:59") >= strtotime(date("Y-m-d H:i:s"))){ ?>
				<div class="col-md-12 text-right">
					<?= lang('already_register')  ?> <a href="<?= base_url().'register' ?>" class="link-black"><b><?= lang('register') ?></b></a>
				</div>
		<?php //} ?>
	</div>
</div>

