<style>
table tr td table th{
	background: #888d97;
	color:#FFF;
}

table tr td table.bordered, table tr td table.bordered tr td{
	border:1px solid #ddd !important;
}
</style>

<table border="0" width="100%">
	<tr>
		<td style="text-align:left">
			<img src="<?= base_url().ASSETS_IMG."logo-cabaran-inklusif.png" ?>" width="50%">
		</td>
		<td style="text-align:right">
			<img src="<?= base_url().ASSETS_IMG."logo-miti.png" ?>" width="30%" style="margin-top:30px;margin-right:10px">
			<img src="<?= base_url().ASSETS_IMG."logo-mosti.png" ?>" width="60%" style="margin-top:30px;margin-left:10px">
			<img src="<?= base_url().ASSETS_IMG."logo-sme.png" ?>" width="60%" style="margin-top:30px;margin-left:10px">
			<img src="<?= base_url().ASSETS_IMG."logo-yim.png" ?>" width="30%" style="margin-top:30px;margin-left:10px">
			<img src="<?= base_url().ASSETS_IMG."logo-ytr.png" ?>" width="30%" style="margin-top:30px;margin-left:10px">
		</td>
	</tr>
</table>

<?php $length = 1000; ?>
<table border="0" width="100%">
	<tr>
		<td colspan="2" style="text-align:center"><h2><?= lang('application_form') ?></h2></td>
	</tr>
	<tr>
		<td width="40%"><?= lang('application_type') ?></td>
		<td width="60%"><?= $application_types[$innovator['application_type']] ?></td>
	</tr>

	<tr>
		<td><?= lang('name') ?></td>
		<td><?= (isset($innovator) ? $innovator['name'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('ic_number') ?></td>
		<td><?= (isset($innovator) ? $innovator['ic_number'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('gender') ?></td>
		<td><?= $gender[$innovator['gender']] ?></td>
	</tr>

	<tr>
		<td><?= lang('address') ?></td>
		<td><?= (isset($innovator) ? $innovator['address'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('postcode') ?></td>
		<td><?= (isset($innovator) ? $innovator['postcode'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('email') ?></td>
		<td><?= (isset($innovator) ? $innovator['email'] : '') ?></td>
	</tr>	

	<tr>
		<td><?= lang('zone') ?></td>
		<td><?= (isset($innovator) ? $innovator['zone'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('state') ?></td>
		<td><?= (isset($innovator) ? $innovator['state']: '') ?></td>
	</tr>

	<tr>
		<td><?= lang('district') ?></td>
		<td><?= (isset($innovator) ? $innovator['district'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('telp_home_no') ?></td>
		<td><?= (isset($innovator) ? $innovator['telp_home_no'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('telp_no') ?></td>
		<td><?= (isset($innovator) ? $innovator['telp_no'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('innovator_photo') ?></td>
		<td><?php if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $innovator['photo']) && $innovator['photo'] != "") { ?>
				<img src="<?= base_url().PATH_TO_INNOVATOR_PHOTO_THUMB.$innovator['photo'] ?>" width="100%"/>
			<?php } ?>
		</td>
	</tr>

	<tr>
		<td colspan="2"><?= lang('team_member') ?></td>
	</tr>

	<?php if($innovator['application_type'] != APPLICATION_TYPE_INDIVIDU){ ?>
	<tr>
		<td colspan="2">
			<table border="1" class="bordered" width="100%">
				<tr>
					<th>No</th>
					<th><?= lang('photo') ?></th>
					<th><?= lang('name') ?></th>
					<th><?= lang('ic_number') ?></th>
				</tr>
			<?php $no=1;
				foreach ($innovator['team_experts'] as $key => $value) { ?>
					<tr>
						<td><?= $no; ?></td>
						<td><?php if(file_exists(realpath(APPPATH . '../'.PATH_TO_TEAM_MEMBER_PHOTO) . DIRECTORY_SEPARATOR . $value['photo']) && $value['photo'] != "") { ?>
									<img src="<?= base_url().PATH_TO_TEAM_MEMBER_PHOTO_THUMB.$value['photo'] ?>" width="200px"/>
							<?php } ?>
						</td>
						<td><?= $value['name']; ?></td>
						<td><?= $value['ic_number']; ?></td>
					</tr>

			<?php $no++;} ?> 
			</table>
		</td>
	</tr>
	<?php } ?>		

	<tr>
		<td><?= lang('referrers') ?></td>
		<td><?= (isset($innovator) ? $referrers[$innovator['referrer']] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('category') ?></td>
		<td><?= $innovation_category[$application['innovation_category']] ?></td>
	</tr>

	<tr>
		<td><?= lang('troubleshooting') ?></td>
		<td><?php foreach ($i_troubleshooting as $val) {
			echo $troubleshootings[$val]."<br/>";
		} ?></td>
	</tr>

	<tr>
		<td><?= lang('innovation_product') ?></td>
		<td><?= (isset($application) ? $application['title'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('inspiration') ?></td>
		<td><?= (isset($application) ? $application['idea'] : '') ?></td>
	</tr>

	<?php 
		$desc_length = ceil(strlen($application['description'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('description')." : " : "") ?> </td>
		<td><?= substr($application['description'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<tr>
		<td><?= lang('estimated_market_size') ?></td>
		<td><?= (isset($application) ? $estimated_market_size[$application['estimated_market_size']] : '') ?></td>
	</tr>

	<?php 
		$desc_length = ceil(strlen($application['material'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('material')." : " : "") ?> </td>
		<td><?= substr($application['material'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$desc_length = ceil(strlen($application['how_to_use'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('how_to_use')." : " : "") ?> </td>
		<td><?= substr($application['how_to_use'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$desc_length = ceil(strlen($application['special_achievement'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('special_achievement')." : " : "") ?> </td>
		<td><?= substr($application['special_achievement'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<tr>
		<td><?= lang('created_date') ?></td>
		<td><?= (isset($application) ? $application['created_date'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('manufacturing_cost') ?></td>
		<td><?= (isset($application) ? $application['manufacturing_cost'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('selling_price') ?></td>
		<td><?= (isset($application) ? $application['selling_price'] : '') ?></td>
	</tr>

	<tr>
		<td><?= lang('income') ?></td>
		<td><?= (isset($application) ? $income[$application['income']] : '') ?></td>
	</tr>	

	<tr>
		<td><?= lang('target') ?></td>
		<td><?php foreach ($i_target as $val) {
			echo $targets[$val]."<br/>";
		} ?></td>
	</tr>

	<tr>
		<td><?= lang('myipo_protection') ?> : </td>
		<td><?= $application['myipo_protection'] == 1 ? lang('yes') : lang('no') ?></td>
	</tr>

	<?php if(count($i_picture) > 0){ ?>
	<tr>
		<td colspan="2"><?= lang('picture') ?></td>
	</tr>
	<?php } ?>

	<?php foreach ($i_picture as $key => $value) { 
			if(file_exists(realpath(APPPATH . '../'.PATH_TO_APPLICATION_PICTURE_THUMB) . DIRECTORY_SEPARATOR . $value['name'])) {
				$url = site_url() . 'assets/attachment/application_picture/thumbnail/' . $value['name'];
				$file_size = getimagesize(PATH_TO_APPLICATION_PICTURE_THUMB . DIRECTORY_SEPARATOR . $value['name']);
			}else{
				$url = site_url() . 'assets/attachment/application_picture/thumbnail/default.jpg';
			} ?>
	<tr>
		<td width="100%" colspan="2"><img src="<?= $url ?>" <?= isset($file_size) ? ($file_size[0] > 600 ? "width='600px'" : '') : '' ?> <?= isset($file_size) ? ($file_size[1] > 600 ? "height='600px'" : '') : '' ?>></td>
	</tr>
	<?php } ?>

</table>