var tableArticle = $('#table-article'),
	formArticle = $('#form-article');

$(function(){
	var table = tableArticle.dataTable({
		"language": {
		    "url": baseUrl+"assets/js/plugins/datatables/i18n/malay.json"
		},
		"bStateSave": true,
	});

	initResetDtState(table);
	initEditor();
	initAlert();
	initValidator();
	initAdd();
	initEdit();
	initDelete();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initAdd(){
	$('.btn-add-article').on('click', function(e){
		window.location.href = adminUrl+'articles/add';
	});

	formArticle.on('click','.btn-cancel', function(){
		window.location.href = adminUrl+'articles';
	});
}

function initValidator(){
	formArticle.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			menu: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			title: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
		}
	});
}

function initEdit(){
	if(edit_mode == 1){
		formArticle.find('.btn-change-pass').removeClass('hide');
		formArticle.find('.collapse-group').addClass('collapse');
	}

	$('.btn-edit-article').on('click', function(e){
		var currentId = $(this).data('id');
		window.location.href = adminUrl+'articles/edit/'+currentId;
	});
}

function initDelete(){
	tableArticle.on('click','.btn-delete-article', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_article,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'articles/delete/'+currentId;
					}
				}
			}
		});
	});
}

function initEditor(){
	$('#content-editor').summernote({
		height:500,
        callbacks : {
            onImageUpload: function(files) {
	            sendFile(files[0]);
	        },
	    }
	});
}

function sendFile(file) {
    var data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url: adminUrl+"articles/upload_images",
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            $('#content-editor').summernote('editor.insertImage', url);
        }
    });
}