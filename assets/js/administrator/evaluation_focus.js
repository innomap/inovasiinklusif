var tableEvaluationFocus = $('#table-evaluation-focus'),
	formEvaluationFocus = $('#form-evaluation-focus'),
	modalEvaluationFocus = $("#modal-evaluation-focus");

$(function(){
	var table = tableEvaluationFocus.dataTable({
		"language": {
		    "url": baseUrl+"assets/js/plugins/datatables/i18n/malay.json"
		},
		"bStateSave": true,
	});

	initResetDtState(table);
	initEditFocus();
	initDeleteFocus();

	initAddCriteria();
	initDeleteCriteria();
});

function initValidatorFocus(){
	formEvaluationFocus.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			label:{
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			percentage:{
				validators: {
					notEmpty: {message: 'The field is required'},
					numeric: {message:'The field value should be numeric'},
				}
			},
			'question[]':{
				group: ".q-item",
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			'description[]':{
				group: ".desc-item",
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			'max_point[]':{
				group: ".point-item",
				validators: {
					numeric: {message:'The field value should be numeric'},
				}
			},
		}
	});
}

function initEditFocus(){
	tableEvaluationFocus.on('click','.btn-edit-focus', function(e){
		formEvaluationFocus.prop('action', adminUrl+'evaluation_focus/update');
		var currentId = $(this).data('id');
		$.get(adminUrl+'evaluation_focus/edit/'+currentId, function(res){
			if(res.status == 1){
				var data = res.data;
				formEvaluationFocus.find('input[name=id]').val(currentId);
				formEvaluationFocus.find('input[name=label]').val(data.label);
				formEvaluationFocus.find('input[name=percentage]').val(data.percentage);
				var str = "";
				var criteria = data.criteria;
				for(i=0;i<criteria.length;i++){
					str += criteriaFieldTemplate(criteria[i].question,criteria[i].description,criteria[i].guideline,criteria[i].max_point,i);
				}

				$('.criteria-wrap').html(str);
				modalEvaluationFocus.modal();
			}
		}, 'json');
	});
	modalEvaluationFocus.on('hidden.bs.modal', function(){
		var str = criteriaFieldTemplate("","","","",0);
		$('.criteria-wrap').html(str);
		formEvaluationFocus.attr('action', adminUrl+'evaluation_form/store')
					.find('input[name=id]').val('');
		formEvaluationFocus.bootstrapValidator('resetForm', true);
		formEvaluationFocus.get(0).reset();
	});
}

function initDeleteFocus(){
	tableEvaluationFocus.on('click','.btn-delete-focus', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name'),
			formId = $(this).data('form-id');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_evaluation_focus,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'evaluation_focus/delete/'+currentId+'/'+formId;
					}
				}
			}
		});
	});
}

function initAddCriteria(){
	formEvaluationFocus.on('click','.add-criteria', function(){
		var str = criteriaFieldTemplate("","","","",1);
		$('.criteria-wrap').append(str);

		formEvaluationFocus.bootstrapValidator('addField', formEvaluationFocus.find('[name="question[]"]'));
		formEvaluationFocus.bootstrapValidator('addField', formEvaluationFocus.find('[name="description[]"]'));
		formEvaluationFocus.bootstrapValidator('addField', formEvaluationFocus.find('[name="max_point[]"]'));
	});
}

function initDeleteCriteria(){
	formEvaluationFocus.on('click','.delete-criteria', function(){
		$(this).parent().parent().remove();
	});
}

function criteriaFieldTemplate(question, description,guideline, max_point, i){
	var str = "";
		str += '<div class="col-md-12 no-padding">';
		str += 		'<div class="col-md-3 no-padding-left q-item">';
		str +=			'<input type="text" name="question[]" class="form-control" placeholder="Kriteria" value="'+(question != "" ? question : "")+'"/>';
		str +=		'</div>';
		str +=		'<div class="col-md-3 no-padding-left desc-item">';
		str +=			'<textarea name="description[]" class="form-control" rows="3" placeholder="Pertanyaan" >'+(description != "" ? description : "")+'</textarea>';
		str +=		'</div>';
		str +=		'<div class="col-md-3 no-padding-left desc-item">';
		str +=			'<textarea name="guideline[]" class="form-control" rows="3" placeholder="Panduan" >'+(guideline != "" ? guideline : "")+'</textarea>';
		str +=		'</div>';
		str +=		'<div class="col-md-2 no-padding-left point-item">';
		str +=			'<input type="text" name="max_point[]" class="form-control no-padding-right" placeholder="Markah Maks" value="'+(question != "" ? max_point : "")+'"/>';
		str +=		'</div>';
		str +=		'<div class="col-md-1 no-padding-left">';
		if(i > 0){
			str +=			'<button type="button" class="btn btn-danger delete-criteria"><span class="fa fa-times"></span></button>';
		}else{
			str +=			'<button type="button" class="btn btn-grey add-criteria"><span class="fa fa-plus"></span></button>';
		}
		str +=		'</div>';
		str +=	'</div>';

	return str;
}