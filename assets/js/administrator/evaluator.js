var tableEvaluator = $('#table-evaluator'),
	formEvaluator = $('#form-evaluator');

$(function(){
	var table = tableEvaluator.dataTable({
		"language": {
		    "url": baseUrl+"assets/js/plugins/datatables/i18n/malay.json"
		},
		"bStateSave": true,
	});

	initResetDtState(table);

	initAlert();
	initValidator();
	initAdd();
	initEdit();
	initDelete();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initAdd(){
	$('.btn-add-evaluator').on('click', function(e){
		window.location.href = adminUrl+'evaluators/add';
	});

	formEvaluator.on('click','.btn-cancel', function(){
		window.location.href = adminUrl+'evaluators';
	});
}

function initValidator(){
	formEvaluator.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			username: {
				validators: {
					notEmpty: {message: 'The username is required'},
					regexp: {
	                    regexp: /^[a-zA-Z0-9_\.]+$/,
	                    message: 'The username can only consist of alphabetical, number, dot and underscore'
	                },
					remote: {
                        message: 'The username is not available',
                        url: baseUrl+'accounts/check_username_exist/',
                        data: function(validator) {
                            return {
                                username: validator.getFieldElements('username').val(),
                                id: validator.getFieldElements('user_id').val()
                            };
                        },
                        type: 'POST'
                    },
				}
			},
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Re-type Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its re-type password are not the same'
	                }
				}
			},
			name: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			evaluator_group_id: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
		}
	});
}

function initEdit(){
	if(edit_mode == 1){
		formEvaluator.find('.btn-change-pass').removeClass('hide');
		formEvaluator.find('.collapse-group').addClass('collapse');
	}

	formEvaluator.on('click','.btn-change-pass',function(e){
		e.preventDefault();
	    var elem = $(this);
	    var collapse = elem.parent().parent().parent().find('.collapse');
	    collapse.collapse('toggle');
	});

	$('.btn-edit-evaluator').on('click', function(e){
		var currentId = $(this).data('id');
		window.location.href = adminUrl+'evaluators/edit/'+currentId;
	});
}

function initDelete(){
	tableEvaluator.on('click','.btn-delete-evaluator', function(e){
		currentId = $(this).data('id');

		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_evaluator,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'evaluators/delete/'+currentId;
					}
				}
			}
		});
	});
}