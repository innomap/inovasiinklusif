$(function () {
    initAnnouncement();
    $('.landing-thumbnail').mosaic();
    $('.mosaic-overlay').hide();
});

function initAnnouncement(){
	bootbox.alert({
        message: lang_announcement
    });
}