var formProfile = $('#form-profile');

$(function () {
        initAlert();
        initEditPassword();
        initImgFancybox();
        initValidator();

        initAddTeamMember();
        initDeleteTeamMember();
        initAutoGenerateGender();
        initCollapseApplicationType();
        initGeocoder();
        initInputTypeFile();
        initDistrictByState();
        initStateByZone();
});

function initAlert() {
        if (alert != '') {
                $('.alert-info').removeClass('hide').hide().fadeIn(500, function () {
                        $(this).delay(3000).fadeOut(500);
                })
        }
}
function initInputTypeFile() {
        $(":file").filestyle({
                iconName: "glyphicon glyphicon-inbox",
                buttonText: "Pilih fail",
                placeholder: "Tiada fail dipilih"
        });
        $(":file").attr('accept="image/x-png, image/x-PNG, image/jpeg, image/jpg, image/JPEG, image/JPG"');
}
function initEditPassword() {
        formProfile.on('click', '.btn-change-pass', function (e) {
                e.preventDefault();
                var elem = $(this);
                var collapse = elem.parent().parent().parent().find('.collapse-password');
                collapse.collapse('toggle');
        });
}

function initImgFancybox() {
        $('.fancyboxs').fancybox({
                padding: 0,
                openEffect: 'elastic',
                helpers: {
                        overlay: {
                                css: {
                                        'background': 'rgba(50, 50, 50, 0.85)'
                                }
                        }
                }
        });
}

function initValidator() {
        $('.number-only').keypress(function (e) {
                var mInput = $(this).val() + "";
                console.log(mInput);
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                }
        });

        var errorMsg = "Input tidak sah";

        formProfile.bootstrapValidator({
                feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                        username: {
                                validators: {
                                        notEmpty: {message: 'The username is required'},
                                        regexp: {
                                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                                message: 'Kata nama hanya boleh terdiri daripada abjad, nombor, titik dan garis bawah'
                                        },
                                        remote: {
                                                message: 'Kata nama sudah wujud',
                                                url: baseUrl + 'accounts/check_username_exist/',
                                                data: function (validator) {
                                                        return {
                                                                username: validator.getFieldElements('username').val(),
                                                                id: validator.getFieldElements('id').val()
                                                        };
                                                },
                                                type: 'POST'
                                        },
                                }
                        },
                        password: {
                                validators: {
                                        notEmpty: {message: errorMsg}
                                }
                        },
                        retype_password: {
                                validators: {
                                        notEmpty: {message: errorMsg},
                                        identical: {
                                                field: 'password',
                                                message: 'Kata laluan tidak sepadan pengesahan'
                                        }
                                }
                        },
                        name: {
                                validators: {
                                        notEmpty: {message: errorMsg}
                                }
                        },
                        ic_num_1: {
                                validators: {
                                        notEmpty: {message: errorMsg},
                                        numeric: {message: errorMsg},
                                        stringLength: {
                                                max: 6,
                                                min: 6,
                                                message: 'Input mesti 6 aksara'
                                        }
                                }
                        },
                        ic_num_2: {
                                validators: {
                                        notEmpty: {message: errorMsg},
                                        numeric: {message: errorMsg},
                                        stringLength: {
                                                max: 2,
                                                min: 2,
                                                message: 'Input mesti 2 aksara'
                                        }
                                }
                        },
                        ic_num_3: {
                                validators: {
                                        notEmpty: {message: errorMsg},
                                        numeric: {message: errorMsg},
                                        stringLength: {
                                                max: 4,
                                                min: 4,
                                                message: 'Input mesti 4 aksara'
                                        }
                                }
                        },
                        address: {
                                validators: {
                                        notEmpty: {message: errorMsg}
                                }
                        },
                        postcode: {
                                validators: {
                                        notEmpty: {message: errorMsg},
                                        numeric: {message: errorMsg},
                                    /*    callback: {
                                                message: errorMsg,
                                                callback: function (value, validator, $field) {
                                                        var result = false;

                                                        if (value >= 25000 && value <= 69000) {
                                                                result = true;
                                                        }/*else if(value == "89999"){	
                                                         result = true;
                                                         }else if(value == "79503"){	
                                                         result = true;
                                                         }*/
                                        /*                return result;
                                                }
                                        }*/
                                }
                        },
                        telp_home_no: {
                                validators: {
                                        numeric: {message: errorMsg},
                                }
                        },
                        telp_no: {
                                validators: {
                                        notEmpty: {message: errorMsg},
                                        numeric: {message: errorMsg},
                                }
                        },
                        district_id: {
                                validators: {
                                        notEmpty: {message: errorMsg}
                                }
                        },
                        team_member_0: {
                                validators: {
                                        notEmpty: {message: errorMsg}
                                }
                        },
                        email: {
                            validators: {
                                emailAddress: {message: errorMsg},
                            }
                        },
                }
        });
}

function initAddTeamMember() {
        var item_num = formProfile.find('.team-member-id').length;

        formProfile.on('click', '.add-team-member', function () {
                str = '<input type="hidden" class="team-member-id" name="team_member_id[]" value="0">';
                str += '<div class="col-md-12 team-member-item no-padding margin-top-10">';
                str += '<input type="hidden" name="h_team_member_pic_' + item_num + '" value="">';
                str += '<input type="hidden" name="i_deleted_team_member_' + item_num + '">';
                str += '<div class="col-md-11 no-padding">';
                str += '<div class="col-md-6 no-padding-left">';
                str += '<input type="text"class="form-control" name="team_member_' + item_num + '" placeholder="Nama">';
                str += '</div>';
                str += '<div class="col-md-6 no-padding-left">';
                str += '<input type="text"class="form-control" name="team_member_ic_num_' + item_num + '" placeholder="Nombor Kad Pengenalan">';
                str += '</div>';
                str += '</div>';
                str += '<div class="col-md-1">';
                str += '<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>';
                str += '</div>';
                str += '<div class="col-md-12 no-padding-left padding-top-10">';
                str += '<div class="col-md-3">';
                str += '<label>Gambar Ahli</label>';
                str += '</div>';
                str += '<div class="col-md-9 no-padding">';
                str += '<input type="file" class="form-control" name="team_member_pic_' + item_num + '">';
                str += '</div>';
                str += '</div>';
                str += '</div>';

                $(".team-member-wrap").append(str);
                item_num++;

                initInputTypeFile();
        });
}

function initDeleteTeamMember() {
        formProfile.on('click', '.delete-team-member', function () {
                $(".team-member-wrap").append("<input type='hidden' name='deleted_team[]' value='" + $(this).data('id') + "'>");
                $(this).parent().parent().remove();
        });
}

function initAutoGenerateGender() {
        formProfile.find('input[name=ic_num_3]').on('keyup', function () {
                var field_val = $(this).val(),
                        ic_no_length = field_val.length;
                if (ic_no_length == 4) {
                        var last_two = parseInt(field_val.substr(ic_no_length - 2));
                        if (last_two % 2 == 1) {
                                //odd
                                $('input[name="gender"][value="0"]').prop('checked', true);
                        } else {
                                //even
                                $('input[name="gender"][value="1"]').prop('checked', true);
                        }
                }
        });
}

function initCollapseApplicationType() {
    formProfile.on('change', 'input[name=application_type]', function (e) {
        var elem = $(this);
        var collapse = formProfile.find('.collapse-team-member'),
         value = $('input[name=application_type]:checked').val();
        
        if(value%2 == 0){
            collapse.hide();
        }else{
            collapse.show();
        }
    });
}

function initGeocoder() {
        var geocoder = new google.maps.Geocoder();
        $('textarea[name=address]').on('blur', function () {
                var address = $(this).val();

                geocoder.geocode({'address': address}, function (results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {
                                var latitude = results[0].geometry.location.lat();
                                var longitude = results[0].geometry.location.lng();

                                $('input[name=latitude]').val(latitude);
                                $('input[name=longitude]').val(longitude);
                        }
                });
        });
}

function initStateByZone(){
    $('#zone-id').on('change', function(){
        $.post(baseUrl+'accounts/get_state_by_zone',{zone_id : $(this).val()}, function(response){
            var state = response.data.state,
                content = "";
            if(state.length > 0){
                content += '<option value="">-- Pilih Negeri --</option>';
                for (var i = 0; i < state.length; i++) {
                    var item = state[i];
                    content += '<option value="'+item.id+'">'+item.name+'</option>'
                }

                $('.state-wrap').html(content);
            }
        },'json');
    });
}

function initDistrictByState(){
    $('#state-id').on('change', function(){
        $.post(baseUrl+'accounts/get_district_by_state',{state_id : $(this).val()}, function(response){
            var district = response.data.district,
                content = "";
            if(district.length > 0){
                content +=      '<option value="">-- Pilih Daerah --</option>';
                for (var i = 0; i < district.length; i++) {
                    var item = district[i];
                    content += '<option value="'+item.id+'">'+item.name+'</option>'
                }

                $('.district-wrap').html(content);
            }
        },'json');
    });
}
