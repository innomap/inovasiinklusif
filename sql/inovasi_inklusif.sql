-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 05, 2016 at 11:49 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inovasi_inklusif`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `ic_number` varchar(20) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `position` varchar(256) NOT NULL,
  `department` varchar(256) NOT NULL,
  `address` mediumtext NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `district_id` int(11) NOT NULL,
  `telp_no` varchar(20) NOT NULL,
  `telp_home_no` varchar(20) NOT NULL,
  `email` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user_id`, `name`, `ic_number`, `gender`, `position`, `department`, `address`, `postcode`, `district_id`, `telp_no`, `telp_home_no`, `email`) VALUES
(3, 13, 'Super Admin', '123456-09-9876', 1, 'xxx', 'xxx', 'xxx', '87123', 1, '124324', '32423', ''),
(8, 24, 'Abdul Razak Ahmad', '123456-34-2443', 0, 'testing', 'testing', 'Johor', '12345', 1, '123214324', '', ''),
(11, 29, 'dsaf', '232354-32-3434', 1, 'ds', 'adsa', 'da', '80000', 1, '34545345', '43543543', ''),
(12, 30, '', '', 0, '', '', '', '', 1, '', '', ''),
(13, 31, '', '', 0, '', '', '', '', 1, '', '', ''),
(14, 32, 'MOHD ARIFF BIN MOHD SAMANI', '830725-01-5027', 0, 'PENOLONG PEGAWAI TEKNOLOGI MAKLUMAT', 'BAHAGIAN SAINS TEKNOLOGI DAN ICT NEGERI JOHOR', 'ARAS 1, BANGUNAN DATO'' JAAFAR MUHAMMAD', '79503', 1, '072666660', '0167408480', ''),
(25, 43, 'Ruzaimi Haron', '900910-01-5185', 0, 'Eksekutif', 'Yayasan Inovasi Malaysia', 'Unit E001, Enterprise Building 1, Block 3440, Jalan Teknokrat 3, Cyberjaya, Selangor', '63000', 1, '0383191714', '0167141589', ''),
(39, 116, '', '', 0, '', '', '', '', 1, '', '', ''),
(41, 416, '', '', 0, '', '', '', '', 1, '', '', ''),
(42, 417, '', '', 0, '', '', '', '', 1, '', '', ''),
(43, 418, '', '', 0, '', '', '', '', 1, '', '', ''),
(44, 460, '', '', 0, '', '', '', '', 1, '', '', ''),
(45, 461, '', '', 0, '', '', '', '', 1, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `idea` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `material` mediumtext NOT NULL,
  `how_to_use` mediumtext NOT NULL,
  `special_achievement` mediumtext NOT NULL,
  `created_date` date NOT NULL,
  `discovered_date` date NOT NULL,
  `manufacturing_cost` varchar(100) NOT NULL,
  `selling_price` varchar(100) NOT NULL,
  `target` varchar(256) NOT NULL,
  `myipo_protection` tinytext NOT NULL,
  `innovation_category` tinyint(1) NOT NULL,
  `troubleshooting` varchar(256) NOT NULL,
  `estimated_market_size` tinyint(1) NOT NULL,
  `income` tinyint(1) NOT NULL,
  `round_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `comment` varchar(255) NOT NULL DEFAULT '',
  `pdf` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `submission_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `round_id` (`round_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`id`, `user_id`, `title`, `idea`, `description`, `material`, `how_to_use`, `special_achievement`, `created_date`, `discovered_date`, `manufacturing_cost`, `selling_price`, `target`, `myipo_protection`, `innovation_category`, `troubleshooting`, `estimated_market_size`, `income`, `round_id`, `status`, `comment`, `pdf`, `submission_date`, `created_at`, `updated_at`) VALUES
(1, 1105, 'Testing', 'Idea', 'keterangan project', 'Bahan Buatan', 'penggunaan', 'testing', '2016-08-09', '2016-08-22', '12', '13', '["1"]', '1', 0, '["0"]', 1, 1, 1, 1, '', '1_Testing.pdf', '2016-08-22 09:26:36', '2016-08-22 09:26:36', '2016-08-22 07:26:43');

-- --------------------------------------------------------

--
-- Table structure for table `application_evaluator`
--

CREATE TABLE IF NOT EXISTS `application_evaluator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `evaluator_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `evaluator_id` (`evaluator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `application_picture`
--

CREATE TABLE IF NOT EXISTS `application_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `application_picture`
--

INSERT INTO `application_picture` (`id`, `application_id`, `name`) VALUES
(1, 1, '1_26784_2_jpg.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `application_status_history`
--

CREATE TABLE IF NOT EXISTS `application_status_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `round_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`,`round_id`),
  KEY `round_id` (`round_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `application_status_history`
--

INSERT INTO `application_status_history` (`id`, `application_id`, `round_id`, `user_id`, `status`, `created_at`) VALUES
(1, 1, 1, 1105, 0, '2016-08-22 07:26:27'),
(2, 1, 1, 1105, 1, '2016-08-22 07:26:37');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `menu`, `title`, `url`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Pengenalan', 'Pengenalan', 'pengenalan', '<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">Sempena Program I-D Johor\r\n2016, Kerajaan Negeri Johor melalui Bahagian Sains Teknologi &amp; ICT Negeri Johor\r\ndengan kerjasama YIM Technology Resources Sdn. Bhd. (YTR) sebagai konsultan\r\nprogram akan mengelolakan <b>Pertandingan Cabaran Inovasi Johor 2016 </b>buat\r\njulung kalinya bermula pada 15 Jun 2016 sehingga 15Julai 2016. </span><span lang="EN-GB"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">Pertandingan ini diadakan\r\nbertujuan untuk memberi peluang kepada rakyat <b>Negeri Johor </b>yang\r\nmempunyai idea <b>kreatif dan inovatif</b> dalam mencipta inovasi yang <b>mampu\r\nmenyelesaikan masalah</b>, baik dalam bentuk teknologi atau produk yang boleh\r\ndikira <b>inklusif</b> sekiranya ia <b>berkualiti tinggi</b>, <b>mampu milik</b>\r\ndan <b>memanfaatkan golongan terpinggir</b> yang ramai, terutamanya mereka yang\r\n<b>pendapatan rendah</b>.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><img src="assets/attachment/article/infographic.png" style="width: 592px;"><br></p>', '2016-06-19 11:07:07', '2016-06-19 03:07:07'),
(2, 'Terma & Syarat ', 'Terma & Syarat', 'terma-dan-syarat', '<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><b><span lang="MS">Terma Penyertaan Cabaran Inovasi Johor 2016</span></b></p><ol><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Peserta\r\nterbuka kepada warga <b>Negeri Johor Darul Ta’zim</b> sama ada <b>menetap</b> atau <b>bekerja</b> di<b>\r\nJohor melebihi 10 tahun</b>.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"><span lang="MS">TIADA HAD</span></b><span lang="MS" style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"> <b>UMUR</b> untuk\r\npenyertaan sama ada secara individu ataupun berkumpulan.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Produk\r\nyang </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">BELUM</b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"> menerima geran dari\r\nmana-mana agensi kerajaan adalah </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">LAYAK </b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">untuk\r\nmenyertai </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Cabaran Inovasi Johor 2016.</b></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="MS" style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Peserta\r\nperlu mendaftar produk inovasi masing-masing di laman sesawang </span><span lang="EN-GB" style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"><a href="http://johor.innomap.my/"><span lang="MS">http://johor.innomap.my</span></a></span><span lang="MS" style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">. Tiada sebarang borang bertulis diedarkan untuk tujuan pendaftaran.\r\nPendaftaran akan disahkan mengikut pejabat daerah masing-masing.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="MS" style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"><span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: ''Times New Roman'';">&nbsp;</span></span><span lang="MS" style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Tarikh\r\nmula mendaftar penyertaan adalah pada <b>15\r\nJun 2016</b> &nbsp;(Rabu, bermula 9.00 pagi)\r\ndan tarikh tutup penyertaan adalah pada <b>15\r\nJulai 2016</b> (Jumaat, berakhir 5.00 petang) Sebarang penyertaan selepas tarikh\r\ntutup ini tidak akan dilayan.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Setiap\r\nindividu atau kumpulan yang berminat layak menyertai </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">LEBIH DARIPADA SATU INOVASI/PROJEK</b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">&nbsp;tanpa sebarang </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">HAD MAKSIMUM</b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Jika\r\nterdapat mana-mana individu atau kumpulan </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">MENARIK\r\nDIRI</b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"> sebelum tarikh tutup yang dinyatakan maka, individu atau kumpulan\r\ntersebut perlu </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">MEMAKLUMKAN</b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"> pihak\r\npenganjur dengan kadar segera.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Sebarang\r\nmaklumat yang diisi adalah benar. Pihak penganjur tidak akan mendedahkan\r\nmaklumat peserta melainkan dengan persetujuan peserta tersebut.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Pihak\r\npenganjur berhak untuk membatalkan mana-mana penyertaan yang menerapkan unsur\r\nnegatif seperti memburukkan kerajaan pusat &amp; negeri, institusi di-raja dan\r\nmana-mana individu, juga penyertaan yang tidak mempunyai berkaitan dengan\r\ninovasi dan rekacipta.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Sebarang\r\nkeputusan pihak penganjur adalah </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">MUKTAMAD</b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">.</span></li></ol><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-align:justify"><b><span lang="MS">&nbsp;</span></b></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-align:justify"><b><span lang="MS">Syarat Penyertaan Cabaran\r\nInovasi Johor 2016</span></b></p><ol><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="font-size: 12pt; text-indent: -18pt; line-height: 1.42857;">Pemenang\r\npertandingan inovasi yang pernah disertai sebelum ini adalah LAYAK untuk\r\nmenyertai </span><b style="font-size: 12pt; text-indent: -18pt; line-height: 1.42857;">Cabaran Inovasi Johor 2016.</b></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Produk\r\ninovasi yang ingin dipertandingkan hendaklah telah mencapai peringkat prototaip\r\ndan bukan di peringkat idea</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Produk\r\ninovasi yang mempunyai tahap penjualan (</span><i style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">sales</i><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">)\r\ntidak mencecah </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">RM50,000</b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"> </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">SETAHUN</b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"> adalah layak untuk menyertai </span><b style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Cabaran Inovasi Johor 2016</b><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">.</span></li></ol>', '2016-06-12 12:35:54', '2016-06-12 04:35:54'),
(3, 'Pertandingan', 'Pertandingan', 'pertandingan', '<p style="text-align: center; "><img src="assets/attachment/article/hadiah pemenang.jpg" style="width: 592px;"><br></p><p style="text-align: center; "><br></p><p style="text-align: center; "><img src="assets/attachment/article/fokus utama.jpg" style="text-align: left; font-size: 12pt; line-height: 1.42857; width: 592px;"></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><img src="assets/attachment/article/kriteria penilaian.jpg" style="width: 592px;"><br></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><br></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><img src="assets/attachment/article/alur.jpg" style="width: 592px;"><br></p>', '2016-06-14 11:39:49', '2016-06-14 03:39:49'),
(4, 'Tentang Kami', 'Tentang Kami', 'tentang-kami', '<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><b><span lang="MS">BAHAGIAN SAINS TEKNOLOGI &amp; ICT NEGERI JOHOR<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">Visi<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">Untuk Menjadi Peneraju Perkhidmatan ICT\r\nYang Terunggul Bagi Memodenkan Perkhidmatan Awam Negeri Johor<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">Objektif<o:p></o:p></span></p><p class="MsoListParagraphCxSpFirst" style="margin-bottom:0cm;margin-bottom:.0001pt;\r\nmso-add-space:auto;text-indent:-18.0pt;mso-list:l1 level1 lfo1"><!--[if !supportLists]--><span lang="MS">1.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Merancang,\r\nMenyelaras, Meningkatkan Dan Memperkembangkan Budaya Penggunaan Teknologi\r\nMaklumat Dalam Pentadbiran dan Pengurusan Kerajaan Negeri.<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l1 level1 lfo1"><!--[if !supportLists]--><span lang="MS">2.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Memberi\r\nPerkhidmatan Teknologi Maklumat Yang Cekap Dan Berkesan Kepada Jabatan Kerajaan\r\nMengikut Perkembangan Teknologi Semasa Yang Sesuai.<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l1 level1 lfo1"><!--[if !supportLists]--><span lang="MS">3.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Mengurus\r\nDan Menyelenggara Infrastruktur ICT, Network Control Centre (NCC), Pangkalan Data\r\nNegeri, Perkakasan, Perisian Dan Aplikasi Sistem.<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l1 level1 lfo1"><!--[if !supportLists]--><span lang="MS">4.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Menganjur\r\nDan Menyelaras Program Pendidikan, Latihan, Kemahiran Dan Penghayatan Sains,\r\nTeknologi dan Teknologi Maklumat.<o:p></o:p></span></p><p class="MsoListParagraphCxSpLast" style="margin-bottom:0cm;margin-bottom:.0001pt;\r\nmso-add-space:auto;text-indent:-18.0pt;mso-list:l1 level1 lfo1"><!--[if !supportLists]--><span lang="MS">5.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Menyelaras\r\ndan Memantau Pelaksanaan Projek MSC dan Program K-Ekonomi Negeri Johor. <o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">Fungsi<o:p></o:p></span></p><p class="MsoListParagraphCxSpFirst" style="margin-bottom:0cm;margin-bottom:.0001pt;\r\nmso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">1.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Memberi\r\nkhidmat nasihat dan perundingan berkaitan teknologi maklumat kepada\r\nJabatan/Agensi Negeri dalam usaha meningkatkan kecekapan dan efisiensi mereka;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">2.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Membuat\r\nkajian, penilaian dan memberi maklumbalas atas cadangan projek pengkomputeran\r\nbaru yang dikemukakan;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">3.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Melaksanakan\r\npembangunan sistem aplikasi komputer dan penyelenggaraan sistem di\r\njabatan/agensi Kerajaan dengan kerjasama pembekal yang telah dilantik;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">4.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Membuat\r\npemantauan dan penyelarasan bagi projek-projek ICT yang dibiayai oleh Kerajaan\r\nNegeri dan Persekutuan. Antaranya adalah seperti berikut:<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><!--[if !supportLists]--><span lang="MS">a.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Sistem\r\nHasil Tanah Johor (SHTJ);<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><!--[if !supportLists]--><span lang="MS">b.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Sistem\r\nPengurusan Perumahan Perumahan (SPPJ);<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><!--[if !supportLists]--><span lang="MS">c.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Sistem\r\nPengurusan Perumahan Lot Bumiputera (Putera);<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><!--[if !supportLists]--><span lang="MS">d.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Sistem\r\nPendaftaran Terbuka (SPT);<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><!--[if !supportLists]--><span lang="MS">e.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">SISPEN,\r\nMyGDI, E-PBT (Teknikal);<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><!--[if !supportLists]--><span lang="MS">f.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Penyelaras\r\nKeselamatan ICT;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><!--[if !supportLists]--><span lang="MS">g.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Pelaksanaan\r\nProjek-projek E-Government dari Kerajaan Persekutuan.<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">5.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Menyelenggara\r\nLaman Web Rasmi Kerajaan Negeri Johor;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">6.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Membuat\r\npemantauan, membantu dan membuat penyelarasan bagi pembangunan Laman Web\r\nJabatan Kerajaan Negeri;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">7.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Merancang,\r\nmelaksana dan menyelenggara jaringan data LAN, WAN (Intranet) dan kemudahan\r\ninternet dan e-mel jabatan;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">8.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Merancang,\r\nmengkaji dan melaksana polisi keselamatan bagi infrastruktur ICT jabatan,\r\nrangkaian intranet dan internet;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">9.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Merancang,melaksana\r\ndan menyelenggara peralatan dan prasarana ICT jabatan;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">10.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Merancang\r\ndan menganjurkan program teknologi maklumat samada berbentuk kursus, latihan,\r\nbengkel, seminar atau pertandingan secara kerjasama dengan jabatan atau pihak\r\nyang dikenalpasti bagi Program Pembudayaan IT;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">11.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Menyediakan\r\ntempat dan tenaga pengajar bagi latihan/kursus komputer<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">12.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Membuat\r\npemantauan serta keberkesanan program-program teknologi maklumat yang telah dan\r\nsedang dilaksanakan di jabatan Kerajaan Negeri melalui Program Audit Pengurusan\r\nTeknologi Maklumat;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">13.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Menjadi\r\nahli dalam Jawatankuasa Pengurusan, Teknikal dan Pengguna bagi sistem-sistem\r\nPengkomputeran yang sedang berjalan;<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0cm;margin-bottom:\r\n.0001pt;mso-add-space:auto;text-indent:-18.0pt;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span lang="MS">14.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Menjadi\r\nurusetia bagi Jawatankuasa Pengkomputeran Negeri Johor seperti:<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><!--[if !supportLists]--><span lang="MS">a.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Jawatankuasa\r\nPemandu ICT Negeri Johor;<o:p></o:p></span></p><p class="MsoListParagraphCxSpLast" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><!--[if !supportLists]--><span lang="MS">b.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang="MS">Jawatankuasa\r\nPerlupusan Peralatan Komputer Negeri Johor</span></p><p class="MsoListParagraphCxSpLast" style="margin-top:0cm;margin-right:0cm;\r\nmargin-bottom:0cm;margin-left:72.0pt;margin-bottom:.0001pt;mso-add-space:auto;\r\ntext-indent:-18.0pt;mso-list:l0 level2 lfo2"><br></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><b><span lang="MS">YAYASAN INOVASI MALAYSIA<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">Yayasan Inovasi Malaysia (YIM) telah\r\nditubuhkan pada 20 Oktober 2008 dan telah diluluskan oleh Kabinet Malaysia pada\r\n7 November 2008.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">Visi<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">Untuk menggalakkan dan\r\nmemupuk kreativiti dan inovasi di kalangan rakyat, terutama mensasarkan\r\nkanak-kanak &amp; remaja, wanita, penduduk luar bandar, orang kurang upaya dan\r\norganisasi bukan kerajaan.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">Misi</span></p><ol><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Membangun\r\ndan menggalakkan kemahiran kreatif dalam bidang sains dan teknologi dalam\r\nbidang akademik, industri dan masyarakat.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Untuk\r\nmemupuk dan menyokong inovasi saintifik di peringkat akar umbi terutamanya di\r\nkalangan belia, wanita dan pertubuhan bukan kerajaan.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Mengadakan\r\nprogram pendidikan dan kesedaran untuk meningkatkan kesedaran terhadap sains\r\ndan teknologi di sekolah dan peringkat akar umbi.</span></li></ol><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="MS"><b>APLIKASI INNOMAP</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">Innomap merupakan satu\r\naplikasi web yang mengumpulkan data inovasi yang telah dijejaki oleh YIM dari\r\nseluruh Malaysia. Ia memudahkan pengguna untuk melihat kembali inovasi yang\r\nterkumpul melalui carian nama inovator, nama inovasi, lokasi, kategori dan\r\nsebagainya.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><br></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">Ciri-ciri Innomap :</span></p><ol><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Pangkalan\r\npengetahuan yang berdasarkan ciptaan ontologi unik yang menghubungkan\r\nkonsep-konsep penting dalam amalan inovasi rakyat Malaysia. Pangkalan maklumat\r\nini boleh diterokai secara visual.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Enjin\r\naliran kerja yang menguruskan proses penapisan data.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Sistem\r\nini mengendalikan proses pelaksanaan pengesahan inovasi melalui maklumbalas oleh\r\n</span><i style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Subject Matter Experts</i><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"> (SMEs) secara\r\ndalam talian.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Enjin\r\ncarian pintar berasaskan semantik yang membolehkan pengguna sistem mencari data\r\nyang saling berkaitan dan menunjukkan hubungan tersembunyi antara &nbsp;data inovasi yang pelbagai.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><i style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"><span lang="MS">Innovation Locator</span></i><span lang="MS" style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;"> membolehkan pengguna sistem mencari inovasi\r\nberdasarkan pada tapisan geografi, padanan kata kunci dan jenis data yang lain.</span></li><li style="margin-bottom: 0.0001pt; text-align: justify;"><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">Modul\r\nLaporan membolehkan pentadbir sistem menjana laporan berformat, paparan\r\nstatistik dan menjalankan </span><i style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">data mining</i><span style="text-indent: -18pt; font-size: 12pt; line-height: 1.42857;">\r\nuntuk tujuan analisa.</span></li></ol>', '2016-06-19 11:06:51', '2016-06-19 03:06:51'),
(5, 'Hubungi Sekretariat', 'Hubungi Sekretariat', 'hubungi-sekretariat', '<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span style="font-size: 12pt; line-height: 1.42857;"><b>Unit Sains Teknologi dan ICT Johor</b></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">Pn. Raja Emy Arfah (07-266 6662 / 013-714 3515)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">En. Ariff (07-266 6679 / 016-740 8480)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><br></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Yayasan Inovasi Malaysia</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span style="font-size: 12pt; line-height: 1.42857;">Pn. Fadzlina binti Mahamad\r\nRazi (03-8319 1714 / 012- 8302125)</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">En. Ruzaimi bin Haron&nbsp; (03-8319 1714 / 016 – 7141 589)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><br></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Pejabat Daerah Johor Bahru</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">En. Mohd Nafis bin Abdul\r\nRazak (017 – 240 8672)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Pejabat Daerah Muar</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">En. Nizam bin Yunus (017 –\r\n754 1893 / 06-952 1021)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Pejabat Daerah Batu Pahat</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">En. Azmee bin Awang (017 –\r\n701 1487 / 07-4311 858)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Pejabat Daerah Segamat</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">YM Ungku Mohd Aliff bin\r\nUngku Farouk (013 – 726 6061 / 07 – 943 5672)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Pejabat Daerah Kluang</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">En. Mustafa Kamal bin\r\nAbdullah (016 – 784 2626 / 07 – 7712 650)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Pejabat Daerah Kota Tinggi</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">Pn. Mysilah Norhazrin binti\r\nHaini (07 – 883 1241 / 013-795 9437)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Pejabat Daerah Pontian</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">En. Anwar Zamil bin Malham\r\n(012 – 7772337 / 07-687 1323)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Pejabat Daerah Mersing</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">En. Mohamad Izzuddin bin\r\nIbrahim (010 – 7000 986 / 07 – 799 1121)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS"><b>Pejabat Daerah Kulai</b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">En. Muhammad Saufee bin\r\nAbdul Shukor (07-662 2573 / 013 – 7733322)<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span lang="MS">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><b><span lang="MS">Pejabat Daerah Tangkak</span>\r\n</b><br class="Apple-interchange-newline"></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:\r\njustify"><span style="font-size: 12pt; line-height: 1.42857;">En. Mohd Syafiq bin Sarju\r\n(013- 622 2685 / 06 – 978 7100)</span></p>', '2016-06-22 09:37:44', '2016-06-22 01:37:44');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('f70f79736d1021bafa132cfb634722e6076df19b', '::1', 1473061288, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036313031363b),
('1575a4d25ecaca06e26b2b4f63afeece0bf44207', '::1', 1473061716, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036313435313b),
('b7b9c979483415221faac41529645d7ced5ea7ed', '::1', 1473062036, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036313737303b),
('c307e8e79c1dc167d830d0f40a22fd0b356a6bec', '::1', 1473062330, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036323037383b),
('bc688d650de76bfc7ff052d2553da9116159c23a', '::1', 1473062735, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036323438353b),
('3e7455951a51f898447fc5ee99bb6422f404fba0', '::1', 1473063111, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036323833353b),
('f6645b2c1a58e17cb93ebb5bb3c86576df47f9f8', '::1', 1473064097, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036333830323b),
('4683f4c74fbfee5b31331cd9f328a4b723178113', '::1', 1473064345, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036343138323b),
('f5d6ce1916f60ec79c95e3d21622e948f60a0be4', '::1', 1473067896, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036373737303b),
('e701423e9a98603af3ae6b1eb2e8a3f9a048dd15', '::1', 1473068299, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036383037323b696e6e6f6d61705f706168616e675f61646d696e7c613a333a7b733a323a226964223b733a323a223133223b733a383a22757365726e616d65223b733a31313a2273757065725f61646d696e223b733a373a22726f6c655f6964223b733a313a2232223b7d),
('a06a89af89e9a002b9fc63e7c2b6c7c7e9361212', '::1', 1473068929, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036383633363b),
('1f38f4360b056828d9cf40ecaf815cc4818193c6', '::1', 1473068947, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437333036383934373b);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`) VALUES
(1, 'Johor Bahru'),
(2, 'Muar'),
(3, 'Batu Pahat'),
(4, 'Segamat'),
(5, 'Kluang'),
(6, 'Kota Tinggi'),
(7, 'Pontian'),
(8, 'Mersing'),
(9, 'Kulai'),
(10, 'Tangkak');

-- --------------------------------------------------------

--
-- Table structure for table `evaluation`
--

CREATE TABLE IF NOT EXISTS `evaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` mediumtext NOT NULL,
  `total` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_criteria`
--

CREATE TABLE IF NOT EXISTS `evaluation_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evaluation_focus_id` int(11) NOT NULL,
  `question` varchar(256) NOT NULL,
  `description` mediumtext NOT NULL,
  `guideline` mediumtext NOT NULL,
  `max_point` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `evaluation_focus_id` (`evaluation_focus_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Dumping data for table `evaluation_criteria`
--

INSERT INTO `evaluation_criteria` (`id`, `evaluation_focus_id`, `question`, `description`, `guideline`, `max_point`) VALUES
(80, 4, 'Harga', 'Adakah inovasi, setelah dikomersilkan, sama atau lebih murah daripada produk atau perkhidmatan yang sedia ada?\r\n\r\nPilih satu sahaja:\r\na) <5% (0 markah)\r\nb) 5 - 20% (2 markah)\r\nc) 21 - 40% (4 markah)\r\nd) 41 - 60% (6 markah)\r\ne) 61 - 80% (8 markah)\r\nf) > 80% (10 markah)', 'Pilih satu sahaja:\r\na) <5% (0 markah)\r\nb) 5 - 20% (2 markah)\r\nc) 21 - 40% (4 markah)\r\nd) 41 - 60% (6 markah)\r\ne) 61 - 80% (8 markah)\r\nf) > 80% (10 markah)', 10),
(81, 4, 'Kaedah Pembayaran', 'Adakah terdapat sebarang mekanisme pembayaran lain untuk menjadikan inovasi mampu milik?\r\n\r\nBerikan 2 markah setiap satu:\r\na) Tunai\r\nb) Ansuran bulanan (cth: koperasi, Courts Mammoth)\r\nc) Kredit\r\nd) bayaran tahunan atau setengah tahun\r\ne) Pajakan', 'Berikan 2 markah setiap satu:\r\na) Tunai\r\nb) Ansuran bulanan (cth: koperasi, Courts Mammoth)\r\nc) Kredit\r\nd) bayaran tahunan atau setengah tahun\r\ne) Pajakan', 10),
(85, 3, 'Nilai Saranan', 'Adakah inovasi menyediakan manfaat yang jelas untuk kumpulan / pengguna sasar?\r\n\r\nPilih satu sahaja:\r\na) Sedikit kurang dalam prestasi (0 markah)\r\nb) Sama (1-4 markah)\r\nc) Sedikit baik dalam prestasi (5-7 markah)\r\nd) Lebih baik dengan ciri-ciri yang dipertingkatkan (8-10 markah)', 'Pilih satu sahaja:\r\na) Sedikit kurang dalam prestasi (0 markah)\r\nb) Sama (4 markah)\r\nc) Sedikit baik dalam prestasi (5-7 markah)\r\nd) Lebih baik dengan ciri-ciri yang dipertingkatkan (8-10 markah)', 10),
(86, 3, 'Perbezaan', 'Adalah inovasi sama atau lebih baik daripada produk atau perkhidmatan yang sedia ada?\r\n\r\nPilih satu sahaja:\r\na) Sedikit kurang dalam prestasi (0 markah)\r\nb) Sama (1-4 markah)\r\nc) Sedikit baik dalam prestasi (5-7 markah)\r\nd) Lebih baik dengan ciri-ciri yang dipertingkatkan (8-10 markah)', 'Pilih satu sahaja:\r\na) Sedikit kurang dalam prestasi (0 markah)\r\nb) Sama (4 markah)\r\nc) Sedikit baik dalam prestasi (5-7 markah)\r\nd) Lebih baik dengan ciri-ciri yang dipertingkatkan (8-10 markah)', 10),
(87, 3, 'Keaslian', 'Adakah inovasi mempunyai sesuatu yang baru yang boleh difailkan?\r\n\r\nPilih satu sahaja:\r\na) Ia tidak mempunyai sesuatu yang baru sekalipun (0 markah)\r\nb) ia boleh difailkan sebagai “Trademark (2 markah)\r\nc) ia boleh difailkan di bawah “Geographical Index” (4 markah)\r\nd) ia boleh difailkan di bawah Reka Bentuk Perindustrian (6 markah)\r\ne) Ia boleh difailkan sebagai Paten (8 markah)', 'Pilih satu sahaja:\r\na) Ia tidak mempunyai sesuatu yang baru sekalipun (0 markah)\r\nb) ia boleh difailkan sebagai “Trademark (2 markah)\r\nc) ia boleh difailkan di bawah “Geographical Index” (4 markah)\r\nd) ia boleh difailkan di bawah Reka Bentuk Perindustrian (6 markah)\r\ne) Ia boleh difailkan sebagai Paten (8 markah)', 10),
(88, 5, 'Golongan Terpinggir', 'Adalah inovasi memberi manfaat kepada golongan terpinggir?\r\n\r\nPilih satu sahaja:\r\na) Ya, nyatakan kumpulan: _____________ (5 markah)\r\nb) Tidak (0 markah)', 'Pilih satu sahaja:\r\na) Ya, nyatakan kumpulan: _____________ (5 markah)\r\nb) Tidak (0 markah)\r\n', 5),
(89, 5, 'Memenuhi Keperluan Tempatan', 'Sejauh manakah inovasi memenuhi keperluan tempatan?\r\n\r\nPilih satu sahaja:\r\na) 20% (2 markah)\r\nb) 40% (4 markah)\r\nc) 60% (6 markah)\r\nd) 80% (8 markah)\r\ne) 100% (10 markah)', 'Pilih satu sahaja:\r\na) 20% (2 markah)\r\nb) 40% (4 markah)\r\nc) 60% (6 markah)\r\nd) 80% (8 markah)\r\ne) 100% (10 markah)', 10),
(90, 5, 'Impak kepada Komuniti', 'Berapa banyak impak inovasi yang akan dibawa kepada komuniti?\r\n\r\nBerikan 2 markah setiap satu:\r\na) Keselamatan\r\nb) Akses Kesihatan\r\nc) Akses Pendidikan\r\nd) Peningkatan Pendapatan\r\ne) Peningkatan Produktiviti', 'Berikan 2 markah setiap satu:\r\na) Keselamatan\r\nb) Akses Kesihatan\r\nc) Akses Pendidikan\r\nd) Peningkatan Pendapatan\r\ne) Peningkatan Produktiviti', 10),
(91, 6, 'Potensi Pasaran', 'Berapakah keluasan potensi pasaran? Bolehkah inovasi disebarkan ke rantau lain cth: negara-negara Asean, Timur Tengah?\r\n\r\nPilih satu sahaja:\r\na) <5% daripada penduduk (2 markah)\r\nb) 5 - 20% daripada penduduk (4 markah)\r\nc) 21 - 50% daripada penduduk (6 markah)\r\nd) > 50% daripada penduduk (8 markah)\r\ne) Boleh dieksport ke negara-negara Asean di mana pasaran boleh ditangani lebih daripada 500,000 (10 markah)\r\n\r\n* Populasi merujuk kepada segmen yang disasarkan', 'Pilih satu sahaja:\r\na) <5% daripada penduduk (2 markah)\r\nb) 5 - 20% daripada penduduk (4 markah)\r\nc) 21 - 50% daripada penduduk (6 markah)\r\nd) > 50% daripada penduduk (8 markah)\r\ne) Boleh dieksport ke negara-negara Asean di mana pasaran boleh ditangani lebih daripada 500,000 (10 markah)\r\n\r\n* Populasi merujuk kepada segmen yang disasarkan', 10),
(92, 6, 'Kategori Pengguna', 'Siapakah pelanggan yang berpotensi untuk inovasi ini?\r\n\r\nBerikan 2 markah setiap satu:\r\na) Kerajaan\r\nb) NGO / persatuan\r\nc) Perniagaan\r\nd) Awam\r\ne) Pembeli asing', 'Berikan 2 markah setiap satu:\r\na) Kerajaan\r\nb) NGO / persatuan\r\nc) Perniagaan\r\nd) Awam\r\ne) Pembeli asing', 10),
(93, 6, 'Kebolehcapaian', 'Adakah mudah untuk kumpulan sasaran untuk mengakses inovasi sebaik sahaja dikomersilkan?\r\n\r\nBerikan 2 markah setiap satu:\r\na) Langsung\r\nb) Runcit\r\nc) Peniaga / Ejen\r\nd) Pengedar', 'Berikan 2 markah setiap satu:\r\na) Langsung\r\nb) Runcit\r\nc) Peniaga / Ejen\r\nd) Pengedar', 10),
(94, 6, 'Jangkamasa', 'Berapa pantas dan mudah boleh inovasi disebarkan?\r\n\r\nBerikan 2 markah setiap satu:\r\na) Melalui perusahaan atau syarikat\r\nb) Melalui “Social Enterprise”\r\nc) Melalui Pertubuhan Bukan Kerajaan\r\nd) Melalui perjanjian pelesenan pihak ke-3\r\ne) Melalui perolehan kerajaan', 'Berikan 2 markah setiap satu:\r\na) Melalui perusahaan atau syarikat\r\nb) Melalui “Social Enterprise”\r\nc) Melalui Pertubuhan Bukan Kerajaan\r\nd) Melalui perjanjian pelesenan pihak ke-3\r\ne) Melalui perolehan kerajaan', 10),
(95, 7, 'Motivasi Inovator', 'Bagaimana sanggup inovator untuk bekerja bersama kita untuk menggerakkan usaha ke hadapan?\r\n\r\nBerikan 2 markah setiap satu:\r\na) Kejujuran dan integriti \r\nb) Sanggup belajar dan menerima nasihat \r\nc) Perkongsian ilmu \r\nd) Dana Peribadi \r\ne) Menghadiri latihan', 'Berikan 2 markah setiap satu:\r\na) Kejujuran dan integriti \r\nb) Sanggup belajar dan menerima nasihat \r\nc) Perkongsian ilmu \r\nd) Dana Peribadi \r\ne) Menghadiri latihan\r\n', 10),
(96, 7, 'Jaringan Strategik', 'Adakah terdapat sebarang jaringan strategik yang boleh kita manfaatkan?\r\n\r\nBerikan 2 markah setiap satu:\r\na) Kementerian Kerajaan\r\nb) Institusi Teknikal \r\nc) Pengedar\r\nd) Fabrikator / Pengeluar\r\n&) Pengeksport', 'Berikan 2 markah setiap satu:\r\na) Kementerian Kerajaan\r\nb) Institusi Teknikal \r\nc) Pengedar\r\nd) Fabrikator / Pengeluar\r\n&) Pengeksport\r\n', 10),
(97, 7, 'Model Perniagaan Daya Maju ', 'Adakah inovasi mempunyai model perniagaan yang berdaya maju untuk membolehkan ia menjadi mampan?\r\n\r\nBerikan 2 markah setiap satu:\r\na) Jualan langsung\r\nb) Pemasaran pelbagai peringkat (MLM)\r\nc) Peruncit\r\nd) Pelesenan\r\ne) OEM atau ODM', 'Berikan 2 markah setiap satu:\r\na) Jualan langsung\r\nb) Pemasaran pelbagai peringkat (MLM)\r\nc) Peruncit\r\nd) Pelesenan\r\ne) OEM atau ODM\r\n', 10);

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_detail`
--

CREATE TABLE IF NOT EXISTS `evaluation_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evaluation_id` int(11) NOT NULL,
  `eval_criteria_id` int(11) NOT NULL,
  `point` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `evaluation_id` (`evaluation_id`),
  KEY `eval_criteria_id` (`eval_criteria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_focus`
--

CREATE TABLE IF NOT EXISTS `evaluation_focus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evaluation_form_id` int(11) NOT NULL,
  `label` varchar(256) NOT NULL,
  `percentage` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `evaluation_form_id` (`evaluation_form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `evaluation_focus`
--

INSERT INTO `evaluation_focus` (`id`, `evaluation_form_id`, `label`, `percentage`) VALUES
(3, 2, 'Kualiti (Quality)', 20),
(4, 2, 'Mampu Milik (Affordability)', 20),
(5, 2, 'Sasaran Golongan Terpinggir (Excluded Group Targeted)', 20),
(6, 2, 'Jangkauan Signifikan (Significant Outreach)', 20),
(7, 2, 'Kemampanan (Sustainability)', 20);

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_form`
--

CREATE TABLE IF NOT EXISTS `evaluation_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `evaluation_form`
--

INSERT INTO `evaluation_form` (`id`, `name`) VALUES
(2, 'Borang Penilaian CIJ2016');

-- --------------------------------------------------------

--
-- Table structure for table `evaluator`
--

CREATE TABLE IF NOT EXISTS `evaluator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `evaluator_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `evaluator_group_id` (`evaluator_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `evaluator`
--

INSERT INTO `evaluator` (`id`, `user_id`, `name`, `evaluator_group_id`) VALUES
(2, 1096, 'Dr. Othman Omar', 1),
(3, 1097, 'Sharmila Salleh', 2),
(4, 1098, 'Azri Mohamad Saleh', 1),
(5, 1099, 'Dr. Azhar Mohamad', 2),
(6, 1100, 'Dr. Mohamad Fakri Zaky Jaafar', 1),
(7, 1101, 'Prof Madya Dr. Norzaidi Mohd Daud ', 2),
(8, 1102, 'Ahmad Azlan Ahmad Shah', 1),
(9, 1103, 'Aminuddin Mohamed', 2),
(10, 1104, 'Evaluator Tester', 3);

-- --------------------------------------------------------

--
-- Table structure for table `evaluator_group`
--

CREATE TABLE IF NOT EXISTS `evaluator_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `evaluator_group`
--

INSERT INTO `evaluator_group` (`id`, `name`, `description`) VALUES
(1, 'Panel Penilai CIJ 2016 Peringkat 1 Kumpulan 1', 'Peringkat 1 merupakan peringkat saringan penyertaan Cabaran Inovasi Johor 2016 yang dihantar bermula 15 Jun 2016 sehingga 15 Julai 2016. 30 inovasi terbaik daripada markah keseluruhan akan dipanggil untuk menyertai Bengkel Penulisan Capturing to Market seterusnya ke Peringkat Akhir iaitu Pitching to Expert.'),
(2, 'Panel Penilai CIJ 2016 Peringkat 1 Kumpulan 2', 'Peringkat 1 merupakan peringkat saringan penyertaan Cabaran Inovasi Johor 2016 yang dihantar bermula 15 Jun 2016 sehingga 15 Julai 2016. 30 inovasi terbaik daripada markah keseluruhan akan dipanggil untuk menyertai Bengkel Penulisan Capturing to Market seterusnya ke Peringkat Akhir iaitu Pitching to Expert.'),
(3, 'Test Group', 'For testing purpose');

-- --------------------------------------------------------

--
-- Table structure for table `innovator`
--

CREATE TABLE IF NOT EXISTS `innovator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `application_type` tinyint(1) NOT NULL,
  `name` varchar(256) NOT NULL,
  `ic_number` varchar(30) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `address` mediumtext NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `telp_home_no` varchar(50) NOT NULL,
  `telp_no` varchar(50) NOT NULL,
  `email` varchar(256) NOT NULL,
  `photo` varchar(256) NOT NULL,
  `district_id` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `innovator`
--

INSERT INTO `innovator` (`id`, `user_id`, `application_type`, `name`, `ic_number`, `gender`, `address`, `postcode`, `telp_home_no`, `telp_no`, `email`, `photo`, `district_id`, `latitude`, `longitude`) VALUES
(1, 1105, 0, 'Kiki Ariyani', '123456-23-2333', 0, 'Jl. Cigadung Raya Timur Komp. Griya Cigadung Baru D-2', '80000', '043534533', '0222512474', 'kiki@mobilus-interactive.com', '1105_1-Bunaken_jpg.jpg', 3, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `innovator_expert_team`
--

CREATE TABLE IF NOT EXISTS `innovator_expert_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `ic_number` varchar(50) NOT NULL,
  `photo` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `round`
--

CREATE TABLE IF NOT EXISTS `round` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `deadline` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `round`
--

INSERT INTO `round` (`id`, `name`, `deadline`) VALUES
(1, 'Peringkat Pertama', '2016-07-15 16:59:59');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `requested_to_delete` tinyint(1) NOT NULL,
  `requested_to_delete_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1106 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `role_id`, `username`, `password`, `status`, `created_at`, `updated_at`, `requested_to_delete`, `requested_to_delete_by`) VALUES
(13, 2, 'super_admin', '209a34280e6416c8d8dac9453bb78e9c', 1, '2016-06-17 14:13:17', '2016-06-20 02:04:54', 0, 0),
(24, 3, 'razak_yim', '96e945f1bb41bf825e0ee0b34fa7b737', 1, '2016-06-13 23:59:27', '2016-06-13 15:59:27', 0, 0),
(29, 2, 'ustict1', '2b07d74fdf2434e3b01b1354d176a28e', 1, '2016-06-17 14:10:33', '2016-06-17 06:10:33', 0, 0),
(30, 2, 'ustict2', 'dda04c1eec508db947237d0f38024196', 1, '2016-06-17 14:10:51', '2016-06-17 06:10:51', 0, 0),
(31, 2, 'ustict3', 'f7c4f4a0c7d74b82a8661b8930add7e1', 1, '2016-06-17 14:11:13', '2016-06-17 06:11:13', 0, 0),
(32, 2, 'ustict4', '8bcad17f0493803012964e5ded767647', 1, '2016-06-19 11:36:34', '2016-06-19 04:18:58', 0, 0),
(43, 3, 'ruzaimi_yim', 'efb7c954158aba4616a7c508a2b8d0ee', 1, '2016-06-10 09:37:40', '2016-06-10 01:37:40', 0, 0),
(115, 4, 'nurhazimah', '7fde1de53a1ed8fa21f17d102dacb17f', 1, '2016-06-17 11:49:37', '2016-06-17 03:49:37', 0, 0),
(116, 2, 'ustict5', '97acbe3ab8143922adab5f50e369c5a0', 1, '2016-06-17 14:12:07', '2016-06-17 06:12:07', 0, 0),
(416, 3, 'fadzlina_yim', '792b64ce4f46449c9f06be9f13d71110', 1, '2016-06-20 11:09:41', '2016-06-20 03:09:41', 0, 0),
(417, 3, 'sharmila_yim', 'ad8da6ddac6d6b7f71eded53a56f168c', 1, '2016-06-20 11:10:09', '2016-06-20 03:10:09', 0, 0),
(418, 3, 'othmandr_yim', '58612b89b58b27ae40e6efc136d8fd69', 1, '2016-06-20 11:56:12', '2016-06-20 03:56:12', 0, 0),
(460, 2, 'ustict6', 'e7d6d59f2c5f27822212792d386bda2a', 1, '2016-06-30 09:50:21', '2016-06-30 01:50:21', 0, 0),
(461, 2, 'ustict7', 'a6ea8299fb3535bdbff5c8b39c933f44', 1, '2016-06-30 09:50:46', '2016-06-30 01:50:46', 0, 0),
(1096, 4, 'othman.dr', '9c05b9c8595183f27c032ec7092e3b7f', 1, '2016-07-22 05:36:03', '2016-07-22 03:36:03', 0, 0),
(1097, 4, 'sharmila', 'beae2174ad0344f0e74b05509f2cc328', 1, '2016-07-19 16:50:55', '2016-07-19 08:50:55', 0, 0),
(1098, 4, 'azri', 'e459474cff5e3615e56fe761c7d8aac7', 1, '2016-07-19 16:47:01', '2016-07-19 08:47:01', 0, 0),
(1099, 4, 'azhar.dr', 'cecd3d7787595326992668dc28459974', 1, '2016-07-19 16:51:44', '2016-07-19 08:51:44', 0, 0),
(1100, 4, 'zaky.dr', '8d1c4bb54d56cba146a5a36532ab870c', 1, '2016-07-19 16:48:02', '2016-07-19 08:48:02', 0, 0),
(1101, 4, 'zaidi.dr', '7e7bcb06e33d4086ae16034a3727ae3c', 1, '2016-07-19 16:52:07', '2016-07-19 08:52:07', 0, 0),
(1102, 4, 'ahmad.azlan', '358beddd88f946246139246fa9f48a7c', 1, '2016-07-18 17:17:37', '2016-07-18 09:17:37', 0, 0),
(1103, 4, 'aminuddin', '3c1ff8059b8be857762012f8d7aeae48', 1, '2016-07-19 17:12:55', '2016-07-19 09:12:55', 0, 0),
(1104, 4, 'evaluator_tester', '87b656d5015967731166712ac40766f8', 1, '2016-07-19 11:05:42', '2016-07-19 03:05:42', 0, 0),
(1105, 0, 'kiki_mobilus', 'b68dd67bd180b27aca888932e0a7036b', 1, '2016-08-22 08:04:17', '2016-08-22 06:04:17', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE IF NOT EXISTS `zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`id`, `name`, `title`, `description`) VALUES
(1, 'Pantai Timur', '28-29 September', 'Cabaran Inovasi Inklusif Zon Pantai Timur (Pahang, Kelantan dan Terengganu)'),
(2, 'Utara', '19-20 October', 'Cabaran Inovasi Inklusif Zon Utara (Perak, Kedah, Pulau Pinang, Perlis)'),
(3, 'Tengah/Selatan', '23-24 November', 'Cabaran Inovasi Inklusif Zon Tengah/Selatan (Kuala Lumpur, Selangor, Putrajaya, Negeri Sembilan, Melaka)'),
(4, 'Sabah & Sarawak', '21-22 December', 'Cabaran Inovasi Inklusif Zon Sabah & Sarawak');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admin_ibfk_2` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `application`
--
ALTER TABLE `application`
  ADD CONSTRAINT `application_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `application_ibfk_2` FOREIGN KEY (`round_id`) REFERENCES `round` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `application_evaluator`
--
ALTER TABLE `application_evaluator`
  ADD CONSTRAINT `application_evaluator_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `application_evaluator_ibfk_2` FOREIGN KEY (`evaluator_id`) REFERENCES `evaluator` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `application_picture`
--
ALTER TABLE `application_picture`
  ADD CONSTRAINT `application_picture_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `application_status_history`
--
ALTER TABLE `application_status_history`
  ADD CONSTRAINT `application_status_history_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `application_status_history_ibfk_2` FOREIGN KEY (`round_id`) REFERENCES `round` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluation`
--
ALTER TABLE `evaluation`
  ADD CONSTRAINT `evaluation_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `evaluation_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `evaluator` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluation_criteria`
--
ALTER TABLE `evaluation_criteria`
  ADD CONSTRAINT `evaluation_criteria_ibfk_1` FOREIGN KEY (`evaluation_focus_id`) REFERENCES `evaluation_focus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluation_detail`
--
ALTER TABLE `evaluation_detail`
  ADD CONSTRAINT `evaluation_detail_ibfk_1` FOREIGN KEY (`evaluation_id`) REFERENCES `evaluation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `evaluation_detail_ibfk_2` FOREIGN KEY (`eval_criteria_id`) REFERENCES `evaluation_criteria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluation_focus`
--
ALTER TABLE `evaluation_focus`
  ADD CONSTRAINT `evaluation_focus_ibfk_1` FOREIGN KEY (`evaluation_form_id`) REFERENCES `evaluation_form` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluator`
--
ALTER TABLE `evaluator`
  ADD CONSTRAINT `evaluator_ibfk_1` FOREIGN KEY (`evaluator_group_id`) REFERENCES `evaluator_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `evaluator_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `innovator`
--
ALTER TABLE `innovator`
  ADD CONSTRAINT `innovator_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `innovator_ibfk_2` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `innovator_expert_team`
--
ALTER TABLE `innovator_expert_team`
  ADD CONSTRAINT `innovator_expert_team_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `innovator` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
